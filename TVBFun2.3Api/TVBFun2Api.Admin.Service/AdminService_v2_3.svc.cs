﻿using Microsoft.ApplicationServer.Caching;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using TVBCOM.SDK.Database;
using TVBFun2Api.Common;
using TVBFun2Api.Common.v1_8.DataModel;
//using TVBFun2Api.Common;

namespace TVBFun2Api.Admin.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.

    /// <summary>
    /// 主要是V2.3的一个后台api，通过Azure 的Job来定时调用这个api（实际上只调用了refreshCache这个api）
    /// 来更新json文件和In-role-cache
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AdminService_v2_3 : AdminService
    {
        //SystemConfigDict systemConfig;
        /********
         * 
        http://tvbdevestorage.blob.core.windows.net/tvbfunjson/system/v1/systemConfig.json

        http://tvbdevestorage.blob.core.windows.net/tvbfunjson/system/v1/forceUpgrade_iOS.json

        http://tvbdevestorage.blob.core.windows.net/tvbfunjson/system/v1/forceUpgrade_android.json

        http://tvbdevestorage.blob.core.windows.net/tvbfunjson/system/v1/forceUpgrade_wp.json
         *
         * ****/
        /// <summary>
        /// 组装由CMS生成的Json文件，最终变成App所需要的Json文件
        /// </summary>
        /// <returns>Final json that app needs</returns>

        private SystemConfigV23 getSystemConfigFromStorage()
        {
            StorageJsonConfigReader reader = new StorageJsonConfigReader();
            //解析CMS的json，包括SystemConfig和3个force upgrade versoin json文件
            SystemConfigV23 systemConfig = reader.ParseSystemConfig(CloudConfigurationManager.GetSetting("LinkOfSystemConfig"));
            SystemConfigV23 iosVersionConfig = reader.ParseVersionConfig(CloudConfigurationManager.GetSetting("LinkOfForceUpgradeIOS"), "IOS");
            SystemConfigV23 androidVersionConfig = reader.ParseVersionConfig(CloudConfigurationManager.GetSetting("LinkOfForceUpgradeAndroid"), "Android");
            SystemConfigV23 wpVersionConfig = reader.ParseVersionConfig(CloudConfigurationManager.GetSetting("LinkOfForceUpgradeWp"), "WP");

            /**
             * 组装CMS的SystemConfig，包括data,force upgrade version
             **/
           // string serverLinksOfProgrammeInfo = systemConfig["zh"]["serverLinksOfProgrammeInfo"].ToString();
           // string serverLinkProgrammeInfo = systemConfig["zh"]["serverLinkProgrammeInfo"].ToString();
            //systemConfig["zh"]["serverLinksOfProgrammeInfo"] = "http://tvbdevestorage.blob.core.windows.net/tvbfundev2/programme_{0}.json";
           // systemConfig["zh"]["serverLinkProgrammeInfo"] = "http://tvbdevestorage.blob.core.windows.net/tvbfundev2/programme_{0}.json";

            systemConfig["en"] = systemConfig["en"].Concat(iosVersionConfig["en"]).ToDictionary(x => x.Key, x => x.Value);
            systemConfig["zh"] = systemConfig["zh"].Concat(iosVersionConfig["zh"]).ToDictionary(x => x.Key, x => x.Value);
            systemConfig["en"] = systemConfig["en"].Concat(androidVersionConfig["en"]).ToDictionary(x => x.Key, x => x.Value);
            systemConfig["zh"] = systemConfig["zh"].Concat(androidVersionConfig["zh"]).ToDictionary(x => x.Key, x => x.Value);
            systemConfig["en"] = systemConfig["en"].Concat(wpVersionConfig["en"]).ToDictionary(x => x.Key, x => x.Value);
            systemConfig["zh"] = systemConfig["zh"].Concat(wpVersionConfig["zh"]).ToDictionary(x => x.Key, x => x.Value);
            

            //生成最后App所需要的Json
            return systemConfig;
        }

        /// <summary>
        /// 更新blob里面的SystemConfig，
        /// 
        /// (事实上V2.3没有调用)
        /// </summary>
        /// <returns></returns>
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                   UriTemplate = "/refreshConfig")]
        public override String RefreshConfig()
        {
            Stopwatch watch = new Stopwatch();
            StringBuilder strb = new StringBuilder();
            watch.Start();
            SystemConfigV23 systemConfig = getSystemConfigFromStorage();
            return PageCommon.UploadFile(systemConfig, CloudConfigurationManager.GetSetting("StorageConnectionString"), CloudConfigurationManager.GetSetting("JSON_v2_3_Folder"), "systemConfig.json", "application/json", false);
            //return "";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                   UriTemplate = "/refreshCache")]

        public override String RefreshCache()
        {
            /*String 字符串常量
              StringBuffer 字符串变量（线程安全）
              StringBuilder 字符串变量（非线程安全）*/
            StringBuilder strb = new StringBuilder();
            Stopwatch watch = new Stopwatch();//测量一个时间间隔的运行时间
            watch.Start();//开始一个计时操作

            //start 更新blob里面V2.3的SystemConfig Json
            //url：	https://tvbstorage.blob.core.windows.net/tvbfun2/systemConfig.json

            SystemConfigV23 systemConfigDict = null;
            try
            {
                systemConfigDict = getSystemConfigFromStorage();
            }
            catch (Exception ex)
            {
                strb.AppendLine(ex.StackTrace);
            }

            try
            {
                if (systemConfigDict != null)
                
              {
                    DeleteFile();
                    strb.AppendLine(PageCommon.UploadFile(systemConfigDict, CloudConfigurationManager.GetSetting("StorageConnectionString"),
                        CloudConfigurationManager.GetSetting("JSON_v2_3_Folder"), "systemConfig.json", "application/json", false));
                }
            }
            catch (Exception ex)
            {
                strb.AppendLine(ex.StackTrace);
            }
            //end 更新blob里面V2.3的SystemConfig Json


            //start 更新blob里面V1.8的SystemConfig Json
            //url：	https://tvbstorage.blob.core.windows.net/jsonv2/SystemConfig.json
            try
            {
                if (systemConfigDict != null)
                {
                    //1.8不支持英文
                    Dictionary<string, object> dict = new Dictionary<string, object>() { 
                        {"sysConfigVar",new SystemConfigV18(systemConfigDict["zh"])}
                     };
                    //StringBuilder.ApppendLine 将默认的行终止符追加到当前 StringBuilder 对象的末尾。
                    strb.AppendLine(PageCommon.UploadFile(dict, CloudConfigurationManager.GetSetting("StorageConnectionString"),
                        CloudConfigurationManager.GetSetting("JSON_v1_8_Folder"), "SystemConfig.json", "application/json", false));
                }
            }
            catch (Exception ex)
            {

                strb.AppendLine(ex.StackTrace);
            }
            //end 更新blob里面V1.8的SystemConfig Json

            //start 更新blob里面V1.8的ProgrammeInfo json，
            //这个Json是用来显示programme list界面
            //url：	https://tvbstorage.blob.core.windows.net/jsonv2/ProgrammeInfo.json
            ProgrammeInfoJsonForApp programmeInfoSet = null;

            try
            {
                if (systemConfigDict != null)
                {
                    programmeInfoSet = ProgrammeInfoJsonForApp.LoadFromDB(CloudConfigurationManager.GetSetting("CMSDBConnection"), CloudConfigurationManager.GetSetting("UrlPrefix"));

                    programmeInfoSet.SysConfigVar = new SystemConfigV18(systemConfigDict["zh"]);
                    strb.AppendLine(PageCommon.UploadFile(programmeInfoSet, CloudConfigurationManager.GetSetting("StorageConnectionString"),
                        CloudConfigurationManager.GetSetting("JSON_v1_8_Folder"), "ProgrammeInfo.json", "application/json", true));
                }
            }

            catch (Exception ex)
            {

                strb.AppendLine(ex.StackTrace);
            }
            //end 更新blob里面V1.8的ProgrammeInfo json，

            //start 更新blob里面V1.8的programmesconfig json，
            //这个Json是当选择一个programme时的一个config，比如里面的option，template
            //url：https://tvbstorage.blob.core.windows.net/tvb-programmesv2/programmesconfig.json

            try
            {
                if (programmeInfoSet != null)
                {
                    ProgrammeInfoJsonForVoting programmeInfoJsonForVoting = new ProgrammeInfoJsonForVoting();

                    programmeInfoJsonForVoting.AddAll(programmeInfoSet.ProgrammeInfoSet);
                    programmeInfoJsonForVoting.AddAll(programmeInfoSet.GroupProgrammeInfoSet);

                    strb.AppendLine(PageCommon.UploadFile(new Dictionary<string, ProgrammeInfoJsonForVoting>()
                    {
                        {"ProgrammeInfoSet",programmeInfoJsonForVoting}
                    },
                    CloudConfigurationManager.GetSetting("StorageConnectionString"),
                    CloudConfigurationManager.GetSetting("JSON_v1_8_VOTING_Folder"), "programmesconfig.json", "application/json", true));
                }
            }
            catch (Exception ex)
            {

                strb.AppendLine(ex.StackTrace);
            }

            try
            {
                ProgrammeInfoJsonForApp todayProgrammeSet = Todayprogramme.LoadFromDB(CloudConfigurationManager.GetSetting("CMSDBConnection"), CloudConfigurationManager.GetSetting("UrlPrefix"));
                //if (todayProgrammeSet != null)
                //{
                //參考舊的json，加一個新的，只取當天programme, 不管時間，放在tvb-programmesv2內，叫todayprogrammes.json
                ProgrammeInfoJsonForVoting programmeInfoJsonForToday = new ProgrammeInfoJsonForVoting();

                programmeInfoJsonForToday.AddAll(todayProgrammeSet.ProgrammeInfoSet);
                programmeInfoJsonForToday.AddAll(todayProgrammeSet.GroupProgrammeInfoSet);
                strb.AppendLine(PageCommon.UploadFile(new Dictionary<string, ProgrammeInfoJsonForVoting>()
                    {
                        {"ProgrammeInfoSet",programmeInfoJsonForToday}
                    },
                CloudConfigurationManager.GetSetting("StorageConnectionString"),
                CloudConfigurationManager.GetSetting("JSON_v1_8_VOTING_Folder"), "todayprogrammes.json", "application/json", true));
                //}
            }
            catch (Exception ex)
            {
                strb.AppendLine(ex.StackTrace);
            }


            //end

            try
            {
                //更新blob里面V2.3的programmesconfig json，
                base.RefreshProgrammes();
                strb.AppendLine(this.GenerateProgrammesStaticFile().ToString());
                this.NotifyFileCopy();
            }
            catch (Exception ex)
            {
                strb.AppendLine(ex.StackTrace);
            }
            //StringBuilder strb = new StringBuilder();
            strb.AppendLine("Completed in " + watch.ElapsedMilliseconds + "V_" + DateTime.Now.ToString("YYYYMMDDHHmmss"));
            return strb.ToString();
        }



        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                   UriTemplate = "/refreshProgrammes")]
        public override Object RefreshProgrammes()
        {
            ProgrammeInfoJsonForApp programmeInfoSet = ProgrammeInfoJsonForApp.LoadFromDB(CloudConfigurationManager.GetSetting("CMSDBConnection"), CloudConfigurationManager.GetSetting("UrlPrefix"));
            programmeInfoSet.SysConfigVar = new SystemConfigV18(getSystemConfigFromStorage()["zh"]);
            //throw new NotImplementedException();
            return programmeInfoSet;
        }

        public void DeleteFile()
        {
            List<String> cloudJsonNameList = Util.DownloadAllFile(CloudConfigurationManager.GetSetting("StorageConnectionString"), CloudConfigurationManager.GetSetting("JSON_v2_3_Folder"));
            //删除过期文件
            if (cloudJsonNameList.Count > 0)
            {
                foreach (var item in cloudJsonNameList)
                {
                        if (item.Equals("systemConfig.json", StringComparison.InvariantCultureIgnoreCase))
                        {
                            Util.DeleteFile(CloudConfigurationManager.GetSetting("StorageConnectionString"), CloudConfigurationManager.GetSetting("JSON_v2_3_Folder"), item);
                        }
                }
            }
        }

     

    }
}
