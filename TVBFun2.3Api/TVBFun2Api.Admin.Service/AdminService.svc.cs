﻿using Microsoft.ApplicationServer.Caching;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using TVBCOM.SDK.Database;
using TVBFun2Api.Common;
//using TVBFun2Api.Common;

namespace TVBFun2Api.Admin.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.

    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AdminService : IAdminService
    {
        string tvbfun2 = CloudConfigurationManager.GetSetting("JSON_v2_3_Folder");
        //string tvbfun2 = "tvbfundev2";//tvbfun2(uat)/tvbfundev2(dev)

        private static readonly DataCacheFactoryConfiguration config = new DataCacheFactoryConfiguration();
        private static readonly DataCacheFactory cacheFactory = new DataCacheFactory(config);
        private static Dictionary<string, string> channels = new Dictionary<string, string>();
        //{
        //    {"翡翠台","1"},
        //    {"J2","2"},
        //    {"互動新聞台","3"},
        //    {"明珠台","4"},
        //    {"高清翡翠台","5"}
        //    //{"J5","6"}
        //};

        /********
         * 
        http://tvbdevestorage.blob.core.windows.net/tvbfunjson/system/v1/systemConfig.json

        http://tvbdevestorage.blob.core.windows.net/tvbfunjson/system/v1/forceUpgrade_iOS.json

        http://tvbdevestorage.blob.core.windows.net/tvbfunjson/system/v1/forceUpgrade_android.json

        http://tvbdevestorage.blob.core.windows.net/tvbfunjson/system/v1/forceUpgrade_wp.json
         *
         * ****/

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                   UriTemplate = "/refreshConfig")]
        public virtual String RefreshConfig()
        {
            Stopwatch watch = new Stopwatch();
            StringBuilder strb = new StringBuilder();
            watch.Start();

            Dictionary<string, Dictionary<string, object>> configSet = new Dictionary<string, Dictionary<string, object>>();
            using (DB db = new DB(DBProvider.BuiltInDBProvider.MSSQL, CloudConfigurationManager.GetSetting("CMSDBConnection")))
            {
                using (IDataReader reader = db.ExecuteReader("select ConfigKey,ConfigValue, Language from SystemConfigView order by Language asc, ConfigKey asc, ID asc ", CommandType.Text))
                {
                    while (reader.Read())
                    {
                        Dictionary<string, object> config = null;
                        if (!configSet.TryGetValue(Convert.ToString(reader["Language"]), out config))
                        {
                            config = new Dictionary<string, object>();
                            configSet.Add(Convert.ToString(reader["Language"]), config);
                        }
                        char[] key = Convert.ToString(reader["ConfigKey"]).ToCharArray();
                        key[0] = Char.ToLower(key[0]);
                        String newKey = new String(key);

                        if (newKey.ToLower().StartsWith("serverlinksof"))
                        {
                            object existsObject = null;
                            if (config.TryGetValue(newKey, out existsObject))
                            {
                                ((List<object>)existsObject).Add(reader["ConfigValue"]);
                            }
                            else
                            {
                                config.Add(newKey, new List<object>() {
                               reader["ConfigValue"]
                            });
                            }
                        }
                        else
                        {
                            config[newKey] = reader["ConfigValue"];
                        }
                    }
                }
            }
            return PageCommon.UploadFile(configSet, CloudConfigurationManager.GetSetting("StorageConnectionString"), tvbfun2, "systemConfig.json", "application/json", false);
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                   UriTemplate = "/refreshCache")]
        public virtual String RefreshCache()
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            TaskFactory f = new TaskFactory();
            f.StartNew(delegate()
            {
                RefreshProgrammes();
                GenerateProgrammesStaticFile();
                NotifyFileCopy();
            });
            f.StartNew(delegate()
            {
                RefreshConfig();
            });

            //StringBuilder strb = new StringBuilder();
            return "Completed in  " + watch.ElapsedMilliseconds+"V_"+DateTime.Now.ToString("YYYYMMDDHHmmss");
        }
        /// <summary>
        /// 根据programmeId生成静态的programme json文件，
        /// </summary>
        /// <returns></returns>
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                   UriTemplate = "/generateProgrammesStaticFile")]
        public virtual Object GenerateProgrammesStaticFile()
        {
            string image_url_cloud = CloudConfigurationManager.GetSetting("image_url_colud");
            string UrlPrefix = CloudConfigurationManager.GetSetting("UrlPrefix");
            if (bool.Parse(CloudConfigurationManager.GetSetting("EnableGenerateStaticFile")))
            {

                StringBuilder strb = new StringBuilder();
                DataCache cache = cacheFactory.GetDefaultCache();
                JObject jObject = (JObject)cache.Get("programmeList");
                if (jObject != null)
                {
                    Object rootProgramme = ProgrammeHelper.FilterById(jObject, 0, false, null);

                    strb.AppendLine(PageCommon.UploadFile(rootProgramme, CloudConfigurationManager.GetSetting("StorageConnectionString"),
                        tvbfun2, "programme_0.json", "application/json", true));

                    List<String> uploadJsonNameList = new List<string>();

                    foreach (var item in jObject)
                    {
                        long pId = Convert.ToInt64(item.Value["programmeId"]);
                        int isAuthPassword = Convert.ToInt32(item.Value["isAuthPassword"]);
                        String basicAuthPassword = Convert.ToString(item.Value["basicAuthPassword"]);
                        String jsonName = PageCommon.GetProgrammeName(isAuthPassword, pId.ToString(), basicAuthPassword);

                        Dictionary<string, object> programme = ProgrammeHelper.FilterById(jObject, pId, false, null);
                        programme.Remove("isAuthPassword");
                        programme.Remove("basicAuthPassword");

                        int listingBannerImageHeight = CheckIntNull(item.Value["listingBannerImageHeight"]);
                        int listingBannerImageWeight = CheckIntNull(item.Value["listingBannerImageWeight"]);
                        int listingBackgroundImageHeight = CheckIntNull(item.Value["listingBackgroundImageHeight"]);
                        int listingBackgroundImageWeight = CheckIntNull(item.Value["listingBackgroundImageWeight"]);
                        int identityBannerImageHeight = CheckIntNull(item.Value["identityBannerImageHeight"]);
                        int identityBannerImageWeight = CheckIntNull(item.Value["identityBannerImageWeight"]);
                        int backgroundFooterImageHeight = CheckIntNull(item.Value["backgroundFooterImageHeight"]);
                        int backgroundFooterImageWeight = CheckIntNull(item.Value["backgroundFooterImageWeight"]);
                        int backgroundImageHeight = CheckIntNull(item.Value["backgroundImageHeight"]);
                        int backgroundImageWeight = CheckIntNull(item.Value["backgroundImageWeight"]);

                        programme.Add("listingBannerImageSize", ConvertToSize(listingBannerImageWeight, listingBannerImageHeight));
                        programme.Add("listingBackgroundImageSize", ConvertToSize(listingBackgroundImageWeight, listingBackgroundImageHeight));
                        programme.Add("identityBannerImageSize", ConvertToSize(identityBannerImageWeight, identityBannerImageHeight));
                        programme.Add("backgroundFooterImageSize", ConvertToSize(backgroundFooterImageWeight, backgroundFooterImageHeight));
                        programme.Add("backgroundImageSize", ConvertToSize(backgroundImageWeight, backgroundImageHeight));

                        programme.Remove("listingBannerImageHeight");
                        programme.Remove("listingBannerImageWeight");
                        programme.Remove("listingBackgroundImageHeight");
                        programme.Remove("listingBackgroundImageWeight");
                        programme.Remove("identityBannerImageHeight");
                        programme.Remove("identityBannerImageWeight");
                        programme.Remove("backgroundFooterImageHeight");
                        programme.Remove("backgroundFooterImageWeight");
                        programme.Remove("backgroundImageHeight");
                        programme.Remove("backgroundImageWeight");

                        strb.AppendLine(PageCommon.UploadFile(programme, CloudConfigurationManager.GetSetting("StorageConnectionString"),
                            tvbfun2, jsonName, "application/json", true));

                        uploadJsonNameList.Add(jsonName);
                        //更新programmeFlag
                        Dictionary<String, object> programmeFlag = new Dictionary<string, object>    
                        {
                                {"gamePageActiveFlag",item.Value["gamePageActiveFlag"]},
                                {"voteActiveFlag",item.Value["voteActiveFlag"]},
                        };

                        strb.AppendLine(PageCommon.UploadFile(programmeFlag, CloudConfigurationManager.GetSetting("StorageConnectionString"),
                            tvbfun2, "programme_" + pId + "_flag.json", "application/json", true));
                    }

                    //获取Bolb上所有的文件
                    List<String> cloudJsonNameList = Util.DownloadAllFile(CloudConfigurationManager.GetSetting("StorageConnectionString"), tvbfun2);
                    //删除过期文件
                    if (cloudJsonNameList.Count > 0)
                    {
                        foreach (var item in cloudJsonNameList)
                        {
                            if (!uploadJsonNameList.Contains(item))
                            {
                                if (item.Equals("systemConfig.json", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    continue;
                                }
                                else if (item.Equals("programme_0.json", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    continue;
                                }
                                else if (item.Contains("_flag.json"))
                                {
                                    continue;
                                }
                                //else if (item.ToLower().Contains("basicauth"))
                                //{
                                //    continue;
                                //}
                                else
                                {
                                    Util.DeleteFile(CloudConfigurationManager.GetSetting("StorageConnectionString"), tvbfun2, item);
                                }
                            }
                        }
                    }

                }
                return strb.ToString();
            }
            else
            {
                return "EnableGenerateStaticFile=false";
            }
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                   UriTemplate = "/notifyFileCopy")]
        public virtual String NotifyFileCopy()
        {
            String notifyFileCopyUrl = CloudConfigurationManager.GetSetting("NotifyFileCopyUrl");
            if (!String.IsNullOrWhiteSpace(notifyFileCopyUrl))
            {
                StringBuilder strb = new StringBuilder(notifyFileCopyUrl);
                HttpWebRequest request = null;
                try
                {
                    request = (HttpWebRequest)HttpWebRequest.Create(notifyFileCopyUrl);
                    request.Method = "GET";
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    strb.AppendLine("Status: " + response.StatusCode);
                    strb.AppendLine("StatusDescription: " + response.StatusDescription);
                    foreach (HttpResponseHeader h in response.Headers)
                    {

                        strb.AppendLine(h + ": " + response.Headers[h]);
                    }

                    //request.GetRequestStream();
                }
                catch (Exception ex)
                {
                    strb.AppendLine(ex.Message);
                }
                finally
                {
                    if (request != null)
                    {
                        request.Abort();
                    }
                }
                return strb.ToString();
            }
            else
            {
                return "notifyFileCopyUrl Not Assigned";
            }
        }

        /// <summary>
        /// 从CMS数据库获取programmelist的完整信息，并保存到In-role-cache中
        /// </summary>
        /// <returns></returns>
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                   UriTemplate = "/refreshProgrammes")]
        public virtual Object RefreshProgrammes()
        {
            string image_url_cloud = CloudConfigurationManager.GetSetting("image_url_colud");
            string UrlPrefix = CloudConfigurationManager.GetSetting("UrlPrefix");
            //http://tvbdevestorage.blob.core.windows.net/
            Stopwatch watch = new Stopwatch();
            StringBuilder strb = new StringBuilder();
            watch.Start();
            //CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
            //    CloudConfigurationManager.GetSetting("StorageConnectionString"));

            //CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            //String urlPrefix=blobClient.BaseUri.AbsoluteUri;
            //<Setting name="UrlPrefix" value="http://tvbclouddevices.tvb.com.hk/" />
            String urlPrefix = CloudConfigurationManager.GetSetting("UrlPrefix");
            strb.AppendLine("Got imageUrlPrefix: " + watch.ElapsedMilliseconds);

            List<Dictionary<string, object>> programmes = new List<Dictionary<string, object>>();
            Dictionary<string, List<object>> optionsSet = new Dictionary<string, List<object>>();
            Dictionary<string, Dictionary<String, object>> OptionPlaceHolderSet = new Dictionary<string, Dictionary<string, object>>();

            using (DB db = new DB(DBProvider.BuiltInDBProvider.MSSQL, CloudConfigurationManager.GetSetting("CMSDBConnection")))
            {
                channels = new Dictionary<string, string>();
                const String sql = "select name,value from ChannelConfig where status=1";
                using (IDataReader readerChannel = db.ExecuteReader(sql, CommandType.Text))
                {
                    while (readerChannel.Read())
                    {
                        channels.Add(readerChannel["name"].ToString(), readerChannel["value"].ToString());
                    }
                }
            }

            using (DB db = new DB(DBProvider.BuiltInDBProvider.MSSQL, CloudConfigurationManager.GetSetting("CMSDBConnection")))
            {
                watch.Restart();
                using (IDataReader reader = db.ExecuteReader("select * from OptionInfoView order by programmeId desc,sortOrder asc", CommandType.Text))
                {
                    while (reader.Read())
                    {
                        List<object> list = null;
                        Dictionary<String, object> dict = parseDictionaryFromReader(reader, urlPrefix);
                        if (!optionsSet.TryGetValue(Convert.ToString(reader["programmeId"]), out list))
                        {
                            list = new List<object>();
                            optionsSet.Add(Convert.ToString(reader["programmeId"]), list);
                        }
                        

                        //把DB裡面的null，改成String.Empty
                        for (int i = 0; i < dict.Count; i++)
                        {

                            string key = dict.Keys.ElementAt(i);
                            if (key.ToLower() == "optionname"
                                || key.ToLower() == "optionimageurl"
                                || key.ToLower() == "optionurl"
                                || key.ToLower() == "optionimageurlchosen"
                                || key.ToLower() == "resultdisplayimage"
                                || key.ToLower() == "basicauthpassword"
                                )
                            {
                                dict[key] = CheckStringNull(dict[key]);
                            }
                            if (key.ToLower() == "optionimageurl")
                            {
                                dict[key] = dict[key].ToString().Replace(UrlPrefix, image_url_cloud);
                            }
                            if (key.ToLower() == "resultdisplayimage")
                            {
                                dict[key] = dict[key].ToString().Replace(UrlPrefix, image_url_cloud);
                            }
                            if (key.ToLower() == "optionimageurlchosen")
                            {
                                dict[key] = dict[key].ToString().Replace(UrlPrefix, image_url_cloud);
                            }
                        }
                        list.Add(dict);
                    }
                }

                strb.AppendLine("Got optionsSet: " + watch.ElapsedMilliseconds);
            }

            using (DB db = new DB(DBProvider.BuiltInDBProvider.MSSQL, CloudConfigurationManager.GetSetting("CMSDBConnection")))
            {
                watch.Restart();
                using (IDataReader reader = db.ExecuteReader("select * from OptionPlaceHolderView order by programmeId desc,sortOrder asc",
                    CommandType.Text))
                {
                    while (reader.Read())
                    {
                        List<object> list = null;
                        Dictionary<String, object> obj = null;
                        if (!OptionPlaceHolderSet.TryGetValue(Convert.ToString(reader["programmeId"]), out obj))
                        {
                            obj = new Dictionary<string, object>();
                            list = new List<object>();
                            obj.Add("type", "");
                            obj.Add("sequence", list);
                            OptionPlaceHolderSet.Add(Convert.ToString(reader["programmeId"]), obj);
                        }
                        else
                        {
                            object _obj;
                            if (obj.TryGetValue("sequence", out _obj))
                            {
                                list = (List<object>)_obj;
                            }
                            else
                            {
                                list = new List<object>();
                            }
                        }
                        list.Add(Path.Combine(image_url_cloud, reader["optionPlaceHolderImage"].ToString()));
                    }
                }

                strb.AppendLine("Got OptionPlaceHolderSet: " + watch.ElapsedMilliseconds);
            }

            using (DB db = new DB(DBProvider.BuiltInDBProvider.MSSQL, CloudConfigurationManager.GetSetting("CMSDBConnection")))
            {
                watch.Restart();
                using (IDataReader reader = db.ExecuteReader("GetProgrammeListWithOrder", CommandType.StoredProcedure))
                {
                    while (reader.Read())
                    {
                        try
                        {

                            Dictionary<string, object> programme = parseDictionaryFromReader(reader, urlPrefix);
                            String programmeId = Convert.ToString(reader["programmeId"]);
                            String alwaysSuccess = reader["alwaysSuccess"].ToString();
                            String isDisplayOptionName = reader["isDisplayOptionName"].ToString();
                            if (alwaysSuccess == "1")
                            {
                                programme["alwaysSuccess"] = true;
                            }
                            if (alwaysSuccess == "0")
                            {
                                programme["alwaysSuccess"] = false;
                            }
                            if (isDisplayOptionName == "1")
                            {
                                programme["isDisplayOptionName"] = true;
                            }
                            if (isDisplayOptionName == "0")
                            {
                                programme["isDisplayOptionName"] = false;
                            }
                            List<object> options = null;
                            if (optionsSet.TryGetValue(programmeId, out options))
                            {
                                programme["voteOptions"] = options;
                            }
                            else
                            {
                                programme["voteOptions"] = new List<object>();
                            }

                            Dictionary<String, object> placeholder = null;
                            if (OptionPlaceHolderSet.TryGetValue(programmeId, out placeholder))
                            {
                                placeholder["type"] = reader["selectOptionsDisplayType"];

                                programme["votePlaceholder"] = placeholder;
                               
                            }
                            else
                            {
                                placeholder = new Dictionary<string, object>();
                                options = new List<object>();
                                placeholder.Add("type", "");
                                placeholder.Add("sequence", options);
                                programme["votePlaceholder"] = placeholder;
                            }


                            //programme["realTimeResultUrl"] = Path.Combine(urlPrefix, Convert.ToString(programme["realTimeResultUrl"]));
                            programmes.Add(programme);


                        }
                        catch (Exception)
                        {

                        }
                    }
                }

                strb.AppendLine("Got programmes: " + watch.ElapsedMilliseconds);
            }

            watch.Restart();

            DataCache cache = cacheFactory.GetDefaultCache();

            //cache.Clear();
            strb.AppendLine("Connected cache server: " + watch.ElapsedMilliseconds);

            watch.Restart();
            cache.Put("programmeList", programmes);
            strb.AppendLine("Write cache: " + watch.ElapsedMilliseconds);

            return strb.ToString();
            //throw new NotImplementedException();
        }

        private static Dictionary<string, object> parseDictionaryFromReader(IDataReader reader, String urlPrefix)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            string image_url_cloud = CloudConfigurationManager.GetSetting("image_url_colud");
            for (int i = 0; i < reader.FieldCount; i++)
            {
                object val = reader[i];
                String key = reader.GetName(i);
                if ((val != null) && (key.ToLower().Contains("url") || key.ToLower().Contains("image")))
                {
                    if (key.ToLower().Contains("weight") == false && key.ToLower().Contains("height") == false)
                    {

                        String url = Convert.ToString(val);
                        if (url.Trim().Length > 0 && !url.ToLower().StartsWith("http"))
                        {
                            url = Path.Combine(urlPrefix, url);
                            val = url;
                        }
                        else if (String.IsNullOrEmpty(url))
                        {
                            val = null;
                        }
                    }
                }
                else if (val != null && key.Equals("channel", StringComparison.OrdinalIgnoreCase))
                {
                    List<String> channelNumList = new List<String>();
                    String[] _channels = val.ToString().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (String channel in _channels)
                    {
                        String channelNum = null;
                        if (channels.TryGetValue(channel.Trim(), out channelNum))
                        {
                            channelNumList.Add(channelNum);
                        }
                    }
                    val = channelNumList;
                }
                else if (val != null && key.Equals("handleVotingErrorCodes", StringComparison.OrdinalIgnoreCase))
                {
                    List<int> errorCodeList = new List<int>();
                    String[] errorCodeArray = val.ToString().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (String errorCode in errorCodeArray)
                    {
                        if (!String.IsNullOrWhiteSpace(errorCode))
                        {
                            errorCodeList.Add(Convert.ToInt32(errorCode.Trim()));
                        }
                    }
                    val = errorCodeList;
                }
                else if (key.Equals("programmeCode", StringComparison.OrdinalIgnoreCase))
                {
                    val = val != null && Convert.ToString(val).Trim().Length > 0 ? val : "general";
                }
                else if (val != null
                    && (
                    key.Equals("serverLinkVoting", StringComparison.OrdinalIgnoreCase)
                    || key.Equals("serverLinksForBuddyPointBalance", StringComparison.OrdinalIgnoreCase)
                    || key.Equals("serverLinksForBuddyPointHistory", StringComparison.OrdinalIgnoreCase)
                    || key.Equals("resultPageMessages", StringComparison.OrdinalIgnoreCase)
                    || key.Equals("resultPageMessagesForWrongAnswer", StringComparison.OrdinalIgnoreCase)
                    )
                    )
                {
                    List<String> serverLinkVotingList = new List<String>();
                    String[] serverLinkVotingArray = val.ToString().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (String serverLinkVoting in serverLinkVotingArray)
                    {
                        if (!String.IsNullOrWhiteSpace(serverLinkVoting))
                        {
                            serverLinkVotingList.Add(serverLinkVoting.Trim());
                        }
                    }
                    val = serverLinkVotingList;
                }

                else if (key.Equals("enableBorder", StringComparison.OrdinalIgnoreCase) || key.Equals("clickable", StringComparison.OrdinalIgnoreCase))
                {
                }
                else if (key.Equals("selectOptionsDisplayType", StringComparison.OrdinalIgnoreCase))
                {
                    continue;
                }
                else if (
                    key.Equals("enableBorder", StringComparison.OrdinalIgnoreCase)
                    || key.Equals("clickable", StringComparison.OrdinalIgnoreCase)
                    || key.Equals("shouldDisplayScore", StringComparison.OrdinalIgnoreCase)
                    || key.Equals("enableCache", StringComparison.OrdinalIgnoreCase)
                    || key.Equals("enableCookies", StringComparison.OrdinalIgnoreCase)
                    || key.Equals("autoRefreshAfterSeconds", StringComparison.OrdinalIgnoreCase)
                    || key.Equals("IsAuthPassword", StringComparison.OrdinalIgnoreCase)
                    )
                {
                    val = CheckIntNull(val);
                }
                if (val != null && val.ToString().ToLower().StartsWith("http"))
                {
                    if ((!val.ToString().ToLower().Contains(".json") && !val.ToString().ToLower().Contains(".html")))
                    {
                        val = val.ToString().Replace(urlPrefix, image_url_cloud);
                    }
                }
                else if(val==null)
                {
                    val = CheckStringNull(val);
                }

                object obj;
                if (!dict.TryGetValue(key, out obj))
                {
                    dict.Add(key, val);
                }
            }

            return dict;
        }

        public static object ConvertToSize(int width, int height)
        {
            return new { width = width, height = height };
        }

        private static int CheckIntNull(object obj)
        {
            int result = 0;
            int.TryParse(obj.ToString(), out result);
            return result; ;
        }

        private static object CheckStringNull(object obj)
        {
            return obj == null ? String.Empty : obj;
        }

    }
}
