﻿using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using TVBCOM.SDK.Net;
using TVBFun2Api.Common;

namespace TVBFun2Api.Admin.Service
{

    public class SystemConfigV23 : Dictionary<String, Dictionary<String, object>>
    {
    
    }

    /******
     * {
platform: "iSO",
minVersion: "2.0.0",
updateUrlZhTw: "https://itunes.apple.com/hk/app/tvb-fun/id450713986?mt=8",
updateUrlEnUs: "https://itunes.apple.com/hk/app/tvb-fun/id450713986?mt=8",
updateMsgZhtw: "無線TVB fun",
updateMsgEnUs: "A new version of TVB fun has been released. Download it now!!!",
lastUpdateDate: 1410489645018,
updatedBy: "ceditor"
}
     * ***/
    public class VersionConfig {
        [JsonProperty("platform")]
        public String Platform { get; set; }
        [JsonProperty("minVersion")]
        public String MinVersion { get; set; }
        [JsonProperty("updateUrlZhTw")]
        public String UpdateUrlZhTw { get; set; }
        [JsonProperty("updateUrlEnUs")]
        public String UpdateUrlEnUs { get; set; }
        [JsonProperty("updateMsgZhtw")]
        public String UpdateMsgZhtw { get; set; }
        [JsonProperty("updateMsgEnUs")]
        public String UpdateMsgEnUs { get; set; }
        [JsonProperty("lastUpdateDate")]
        public long LastUpdateDate { get; set; }
        [JsonProperty("updatedBy")]
        public String UpdatedBy { get; set; }
    }

    public class StorageJsonConfigReader
    {


        //private CloudStorageAccount storageAccount;
        //private CloudBlobClient blobClient;
        //public StorageJsonConfigReader(String azureStorage, Type configParserType)
        //{

        //    //storageAccount = CloudStorageAccount.Parse(azureStorage);

        //    //blobClient = storageAccount.CreateCloudBlobClient();

        //}

        public SystemConfigV23 ParseSystemConfig(String configFileUrl)
        {
           String json= HttpLoader.Get(configFileUrl);

            JsonSerializer serializer = new JsonSerializer();
            SystemConfigV23 jObject = JsonConvert.DeserializeObject<SystemConfigV23>(json);
            return jObject;
                        
        }

        public SystemConfigV23 ParseVersionConfig(String configFileUrl, String platform)
        {
            String json = HttpLoader.Get(configFileUrl);
            JsonSerializer serializer = new JsonSerializer();
            VersionConfig  versionConfigObject = JsonConvert.DeserializeObject<VersionConfig>(json);
            SystemConfigV23 jObject = new SystemConfigV23() { 
                {
                    "zh", new Dictionary<String, Object>(){
                        {String.Format("min{0}Version", platform), versionConfigObject.MinVersion},
                        {String.Format("{0}VersionUpdateMessage", platform.ToLower()), versionConfigObject.UpdateMsgZhtw},
                        {String.Format("{0}VersionUpdateUrl", platform.ToLower()), versionConfigObject.UpdateUrlZhTw}
                    }
                },
                {
                    "en", new Dictionary<String, Object>(){
                        {String.Format("min{0}Version", platform), versionConfigObject.MinVersion},
                        {String.Format("{0}VersionUpdateMessage", platform.ToLower()), versionConfigObject.UpdateMsgEnUs},
                        {String.Format("{0}VersionUpdateUrl", platform.ToLower()), versionConfigObject.UpdateUrlEnUs}
                    }
                }
            };
            return jObject;
        }
    }
}