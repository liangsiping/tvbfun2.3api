﻿using Microsoft.WindowsAzure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TVBFun2Api.Common;

namespace TVBFun2Api.Admin.Service
{
    public class PageCommon
    {
        public static String StorageConnectionString = CloudConfigurationManager.GetSetting("StorageConnectionString");
        public static String StorageConnectionStringBak = CloudConfigurationManager.GetSetting("StorageConnectionStringBak");

        public static String FTPStorageHost = CloudConfigurationManager.GetSetting("FTPStorageHost");
        public static String FTPStorageUser = CloudConfigurationManager.GetSetting("FTPStorageUser");
        public static String FTPStoragePassword = CloudConfigurationManager.GetSetting("FTPStoragePassword");

        public static String JSON_v2_3_Folder = CloudConfigurationManager.GetSetting("JSON_v2_3_Folder");
        public static String JSON_v1_8_Folder = CloudConfigurationManager.GetSetting("JSON_v1_8_Folder");
        public static String JSON_v1_8_VOTING_Folder = CloudConfigurationManager.GetSetting("JSON_v1_8_VOTING_Folder");

        public static String LinkOfSystemConfig = CloudConfigurationManager.GetSetting("LinkOfSystemConfig");
        public static String LinkOfForceUpgradeIOS = CloudConfigurationManager.GetSetting("LinkOfForceUpgradeIOS");
        public static String LinkOfForceUpgradeAndroid = CloudConfigurationManager.GetSetting("LinkOfForceUpgradeAndroid");
        public static String LinkOfForceUpgradeWp = CloudConfigurationManager.GetSetting("LinkOfForceUpgradeWp");

        public static String EncryptProgrammeJsonNameFormat = CloudConfigurationManager.GetSetting("EncryptProgrammeJsonNameFormat");
        //public static String EnableUploadFTP = CloudConfigurationManager.GetSetting("EnableUploadFTP");


        public static String UploadFile(object data, string connectionString, string containerName, string fileName, string mimeType, bool enableUploadBak)
        {
            StringBuilder result = new StringBuilder();
            //Stopwatch watch = new Stopwatch();
            //watch.Start();

            List<Task> tasks = new List<Task>();

            Task task1 = new Task(() =>
            {
                // watch.Restart();
                result.Append(Util.UploadFile(data, PageCommon.StorageConnectionString, containerName, fileName, mimeType));
                //    result.Append("task1 spend ms" + watch.ElapsedMilliseconds);
            });
            task1.Start();
            tasks.Add(task1);

            if (CloudConfigurationManager.GetSetting("EnableUploadBak").ToLower().Trim() == "true")
            {
                if (enableUploadBak == true)
                {
                    Task task2 = new Task(() =>
                    {
                        //   watch.Restart();
                        Util.UploadFile(data, PageCommon.StorageConnectionStringBak, containerName, fileName, mimeType);
                        //  result.Append("task2 spend ms" + watch.ElapsedMilliseconds);
                    });
                    task2.Start();
                    tasks.Add(task2);
                }
            }

            if (CloudConfigurationManager.GetSetting("EnableUploadFTP").ToLower().Trim() == "true")
            {
                Task task3 = new Task(() =>
                 {
                     // watch.Restart();
                     FTPUtil.UploadFile(data, PageCommon.FTPStorageHost, PageCommon.FTPStorageUser, PageCommon.FTPStoragePassword, containerName, fileName);
                     // result.Append("task3 spend ms" + watch.ElapsedMilliseconds);
                 });
                task3.Start();
                tasks.Add(task3);
            }

            Task.WaitAll(tasks.ToArray());

            return result.ToString();
        }

        public static String GetProgrammeName(int isAuthPassword, String pId, String basicAuthPassword)
        {
            String result = "";

            if (isAuthPassword == 0)
            {
                result = "programme_" + pId + ".json";
            }
            else
            {
                String EncodeValue = ValueConvert.GetHMACSHA1Value(basicAuthPassword, pId);
                result = PageCommon.EncryptProgrammeJsonNameFormat.Replace("12345", pId).Replace("32e9c1585a8527160951a580226ef9ad69b4667c", EncodeValue);
            }

            return result;
        }
    }
}