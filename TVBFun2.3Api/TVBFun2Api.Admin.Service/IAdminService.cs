﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace TVBFun2Api.Admin.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IAdminService
    {
        [OperationContract]
        String RefreshConfig();

        [OperationContract]
        String RefreshCache();

        [OperationContract]
        Object RefreshProgrammes();

        [OperationContract]
        String NotifyFileCopy();

        [OperationContract]
        Object GenerateProgrammesStaticFile();
        // TODO: Add your service operations here
    }

}
