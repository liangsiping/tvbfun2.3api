﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TVBFun2Api.Admin.Service
{
    [AttributeUsage(AttributeTargets.Method)]
    public class JsonConfigReaderAttribute : Attribute
    {
        public String ContainerName {get;set;}
        public String FileName { get; set; }
        public Type InputJsonType { get; set; }
    }
}