﻿using Microsoft.ApplicationServer.Caching;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using TVBCOM.SDK.Database;
using TVBFun2Api.Common;

namespace TVBFun2Api.Admin.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.

    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AdminService : IAdminService
    {
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                   UriTemplate = "/refreshcache")]
        public String RefreshCache()
        {
            //http://tvbdevestorage.blob.core.windows.net/


            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                CloudConfigurationManager.GetSetting("StorageConnectionString"));

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            String urlPrefix=blobClient.BaseUri.AbsoluteUri;

            JsonList programmes = new JsonList();
            using (DB db = new DB("CMSDB"))
            {
                Dictionary<string, JsonList> optionsSet = new Dictionary<string, JsonList>();
                using (IDataReader reader = db.ExecuteReader("select * from OptionInfoView order by programmeId desc", CommandType.Text))
                {
                    while (reader.Read()) {
                        JsonList list = null;
                        if (!optionsSet.TryGetValue(Convert.ToString(reader["programmeId"]), out list)) {
                            list = new JsonList();
                            optionsSet.Add(Convert.ToString(reader["programmeId"]), list);
                        }
                        list.Add(parseDictionaryFromReader(reader, urlPrefix));
                    }
                }

                using (IDataReader reader = db.ExecuteReader("select * from ProgrammeInfoView order by programmeId asc", CommandType.Text))
                {
                    while (reader.Read()) {
                        JsonDictionary programme = parseDictionaryFromReader(reader, urlPrefix);
                        JsonList options = null;
                        if (optionsSet.TryGetValue(Convert.ToString(reader["programmeId"]), out options))
                        {
                            programme["voteOptions"] = options;
                        }
                        else {
                            programme["voteOptions"] = new JsonList();
                        }
                        programmes.Add(programme);
                    }                
                }

            }

            DataCache cache = new DataCache();
            cache.Put("programmeList", programmes);

            return "success";
            //throw new NotImplementedException();
        }

        private static JsonDictionary parseDictionaryFromReader(IDataReader reader, String urlPrefix)
        {
            JsonDictionary dict = new JsonDictionary();
            for (int i = 0; i < reader.FieldCount; i++) {
                object val = reader[i];
                if (val!=null && reader.GetName(i).ToLower().EndsWith("imageurl")) {
                    String url = Convert.ToString(val);
                    if (!url.ToLower().StartsWith("http")) {
                        url = Path.Combine(urlPrefix, url);
                    }
                }

                dict.Add(reader.GetName(i), reader[i]);
            }
            return dict;
        }
    }
}
