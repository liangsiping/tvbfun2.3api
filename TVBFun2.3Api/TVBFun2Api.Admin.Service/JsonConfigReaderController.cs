﻿using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace TVBFun2Api.Admin.Service
{
    public class JsonConfigReaderController
    {
        private CloudStorageAccount storageAccount;
        private CloudBlobClient blobClient;
        private Object configParserTypeInstance;
        public JsonConfigReaderController(String azureStorage, Type configParserType) {

            storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));

            blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve reference to a previously created container.
            configParserTypeInstance = Activator.CreateInstance(configParserType);
        }

        public void Read(String containerName, String fileName, Dictionary<string, Dictionary<string, object>> result)
        {
            CloudBlobContainer container = blobClient.GetContainerReference(containerName);

            if (container.CreateIfNotExists())
            {
                var permission = container.GetPermissions();
                permission.PublicAccess = BlobContainerPublicAccessType.Container;
                container.SetPermissions(permission);
            }
            IEnumerable<IListBlobItem> blobList = container.ListBlobs();
            foreach (var item in blobList)
            {
                if (typeof(CloudBlockBlob).IsInstanceOfType(item))
                {
                    CloudBlockBlob blobItem = (CloudBlockBlob)item;
                    
                    String jsonText = blobItem.DownloadText(Encoding.UTF8);

                    foreach (var method in configParserTypeInstance.GetType().GetMethods()) {
                        foreach(var attr in method.GetCustomAttributes(typeof(JsonConfigReaderAttribute),true)){
                            if(typeof(JsonConfigReaderAttribute).IsInstanceOfType(attr) && method.ReturnType.IsInstanceOfType(result) ){
                               JsonConfigReaderAttribute  jsonAttr=(JsonConfigReaderAttribute)attr;
                               method.Invoke(configParserTypeInstance, new Object[] { jsonText });
                            }
                        }
                    }

                }
            }
           
        }
        public Dictionary<string, Dictionary<string, object>> Read(String container, String fileName)
        {
            return null;
        }
    }
}