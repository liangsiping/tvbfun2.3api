﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace TVBFun2Api.Common
{
    public class FTPUtil
    {
         private static int poolSize = 3;
        private static int timeInterval = 1500;
        private static Dictionary<String, List<FtpWebRequest>> dic = new Dictionary<string, List<FtpWebRequest>>();

        static FTPUtil()
        {
            //dic.Add(@"waws-prod-hk1-003.ftp.azurewebsites.windows.net*ibmftp\ibmftpuser*IBMFTP2015*test*test0_.json", new List<FtpWebRequest>());

            try
            {
                System.Timers.Timer tmr = new System.Timers.Timer();
                tmr.Interval = timeInterval;
                tmr.Elapsed += tmr_Elapsed;
                tmr.Start();

                //new Task(() =>
                //{
                //    while (true)
                //    {
                //        List<FtpWebRequest> list;
                //        for (int i = 0; i < dic.Count; i++)
                //        {
                //            list = dic.ElementAt(i).Value;
                //            if (list.Count < poolSize)
                //            {
                //                String[] keys = dic.ElementAt(i).Key.Split('*');
                //                if (keys.Length == 5)
                //                {
                //                    list.Add(CreateFtpWebRequest(keys[0], keys[1], keys[2], keys[3], keys[4]));
                //                }
                //            }
                //        }
                //    }
                //}).Start();
            }
            catch (Exception)
            {
                throw;
            }
        }

        static void tmr_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            lock (dic)
            {
                List<FtpWebRequest> list;
                for (int i = 0; i < dic.Count; i++)
                {
                    list = dic.ElementAt(i).Value;
                    if (list.Count < poolSize)
                    {
                        String[] keys = dic.ElementAt(i).Key.Split('*');
                        if (keys.Length == 5)
                        {
                            list.Add(CreateFtpWebRequest(keys[0], keys[1], keys[2], keys[3], keys[4]));
                        }
                    }
                }
            }
        }

        private static FtpWebRequest GetFtpWebRequest(string host, string userName, string password, string containerName, string fileName)
        {
            FtpWebRequest result = null;
            String key = host + "*" + userName + "*" + password + "*" + containerName + "*" + fileName;

            List<FtpWebRequest> list = null;
            if (dic.TryGetValue(key, out list))
            {
                if (list.Count > 1)
                {
                    result = list[0];
                    list.RemoveAt(0);
                }
                else
                {
                    result = CreateFtpWebRequest(host, userName, password, containerName, fileName);
                }
            }
            else
            {
                result = CreateFtpWebRequest(host, userName, password, containerName, fileName);
                dic.Add(key, new List<FtpWebRequest>());
            }

            return result;
        }

        private static FtpWebRequest CreateFtpWebRequest(string host, string userName, string password, string containerName, string fileName)
        {
            string uri = "ftp://" + host + "/" + containerName + "/";
            CheckDirectory(userName, password, uri);
            uri += fileName;
            // 根据uri创建FtpWebRequest对象 
            FtpWebRequest reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(uri));
            reqFTP.Timeout = 60 * 1000;
            reqFTP.KeepAlive = false;
            // ftp用户名和密码
            reqFTP.Credentials = new NetworkCredential(userName, password);

            // 默认为true，连接不会被关闭
            // 在一个命令之后被执行
            reqFTP.KeepAlive = false;

            // 指定执行什么命令
            reqFTP.Method = WebRequestMethods.Ftp.UploadFile;

            // 指定数据传输类型
            reqFTP.UseBinary = true;

            return reqFTP;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="host"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="containerName"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string UploadFile(object data, string host, string userName, string password, string containerName, string fileName)
        {
            // 上传文件时通知服务器文件的大小
            //reqFTP.ContentLength = fileInf.Length;

            // 缓冲大小设置为2kb
            int buffLength = 2048 * 5;

            byte[] buff = new byte[buffLength];
            int contentLen;

            Encoding utf8Encoding = new UTF8Encoding(false);
            using (MemoryStream stream = new MemoryStream())
            {
                JsonSerializer serializer = new JsonSerializer();
                using (TextWriter writer = new StreamWriter(stream, utf8Encoding))
                {
                    serializer.Serialize(writer, data);
                    writer.Flush();
                    stream.Seek(0, SeekOrigin.Begin);

                    // 把上传的文件写入流

                    Stream strm = GetFtpWebRequest(host, userName, password, containerName, fileName).GetRequestStream();

                    // 每次读文件流的2kb
                    contentLen = stream.Read(buff, 0, buffLength);

                    // 流内容没有结束
                    while (contentLen != 0)
                    {
                        // 把内容从file stream 写入 upload stream
                        strm.Write(buff, 0, contentLen);

                        contentLen = stream.Read(buff, 0, buffLength);
                    }
                    // 关闭两个流
                    strm.Close();
                }
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pFtpUserID"></param>
        /// <param name="pFtpPW"></param>
        /// <param name="FileSource"></param>
        public static void CheckDirectory(string pFtpUserID, string pFtpPW, string FileSource)
        {
            //检测目录是否存在
            Uri uri = new Uri(FileSource);
            if (!DirectoryIsExist(uri, pFtpUserID, pFtpPW))
            {
                CreateDirectory(uri, pFtpUserID, pFtpPW);
            }
        }

        /// <summary>
        /// 检测目录是否存在
        /// </summary>
        /// <param name="ServerIP"></param>
        /// <param name="pFtpUserID"></param>
        /// <param name="pFtpPW"></param>
        /// <param name="FileSource"></param>
        /// <param name="FileCategory"></param>
        /// <returns></returns>
        public static bool CheckDirectory(string ServerIP, string pFtpUserID, string pFtpPW, string FileSource, string FileCategory)
        {
            //检测目录是否存在
            Uri uri = new Uri("ftp://" + ServerIP + "/" + FileSource + "/");
            if (!DirectoryIsExist(uri, pFtpUserID, pFtpPW))
            {
                //创建目录
                uri = new Uri("ftp://" + ServerIP + "/" + FileSource);
                if (CreateDirectory(uri, pFtpUserID, pFtpPW))
                {
                    //检测下一级目录是否存在
                    uri = new Uri("ftp://" + ServerIP + "/" + FileSource + "/" + FileCategory + "/");
                    if (!DirectoryIsExist(uri, pFtpUserID, pFtpPW))
                    {
                        //创建目录
                        uri = new Uri("ftp://" + ServerIP + "/" + FileSource + "/" + FileCategory);
                        if (CreateDirectory(uri, pFtpUserID, pFtpPW))
                        {
                            return true;
                        }
                        else { return false; }
                    }
                    else
                    {
                        return true;
                    }
                }
                else { return false; }
            }
            else
            {
                //检测下一级目录是否存在
                uri = new Uri("ftp://" + ServerIP + "/" + FileSource + "/" + FileCategory + "/");
                if (!DirectoryIsExist(uri, pFtpUserID, pFtpPW))
                {
                    //创建目录
                    uri = new Uri("ftp://" + ServerIP + "/" + FileSource + "/" + FileCategory);
                    if (CreateDirectory(uri, pFtpUserID, pFtpPW))
                    {
                        return true;
                    }
                    else { return false; }
                }
                else
                {
                    return true;
                }
            }
        }
        /// <summary>
        /// ftp创建目录(创建文件夹)
        /// </summary>
        /// <param name="IP">IP服务地址</param>
        /// <param name="UserName">登陆账号</param>
        /// <param name="UserPass">密码</param>
        /// <param name="FileSource"></param>
        /// <param name="FileCategory"></param>
        /// <returns></returns>
        public static bool CreateDirectory(Uri IP, string UserName, string UserPass)
        {
            try
            {
                FtpWebRequest FTP = (FtpWebRequest)FtpWebRequest.Create(IP);
                FTP.Credentials = new NetworkCredential(UserName, UserPass);
                FTP.Proxy = null;
                FTP.KeepAlive = false;
                FTP.Method = WebRequestMethods.Ftp.MakeDirectory;
                FTP.UseBinary = true;
                FtpWebResponse response = FTP.GetResponse() as FtpWebResponse;
                response.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// 检测目录是否存在
        /// </summary>
        /// <param name="pFtpServerIP"></param>
        /// <param name="pFtpUserID"></param>
        /// <param name="pFtpPW"></param>
        /// <returns>false不存在，true存在</returns>
        public static bool DirectoryIsExist(Uri pFtpServerIP, string pFtpUserID, string pFtpPW)
        {
            string[] value = GetFileList(pFtpServerIP, pFtpUserID, pFtpPW);
            if (value == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public static string[] GetFileList(Uri pFtpServerIP, string pFtpUserID, string pFtpPW)
        {
            StringBuilder result = new StringBuilder();
            try
            {
                FtpWebRequest reqFTP = (FtpWebRequest)FtpWebRequest.Create(pFtpServerIP);
                reqFTP.UseBinary = true;
                reqFTP.Credentials = new NetworkCredential(pFtpUserID, pFtpPW);
                reqFTP.Method = WebRequestMethods.Ftp.ListDirectoryDetails;

                WebResponse response = reqFTP.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string line = reader.ReadLine();
                while (line != null)
                {
                    result.Append(line);
                    result.Append("\n");
                    line = reader.ReadLine();
                }
                reader.Close();
                response.Close();
                return result.ToString().Split('\n');
            }
            catch
            {
                return null;
            }
        }
    }
}
