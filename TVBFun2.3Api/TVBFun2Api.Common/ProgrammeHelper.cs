﻿using Microsoft.WindowsAzure;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TVBFun2Api.Common
{
    public class ProgrammeHelper
    {
        public static Dictionary<string, object> FilterById(JObject jObject, long programmeId, bool filterGroups, string groups)
        {
            string image_url_cloud = CloudConfigurationManager.GetSetting("image_url_colud");
            string UrlPrefix = CloudConfigurationManager.GetSetting("UrlPrefix");
            //JObject jObject = (JObject)cache.Get("programmeList");
            Dictionary<string, object> parentProgram = null;
            List<object> filterList = new List<object>();
            int displayGroup=0;
            int gameMode = 0;
            if (jObject != null)
            {
                List<String> _groups = new List<String>();
                if (!String.IsNullOrEmpty(groups))
                {
                    _groups.AddRange(groups.Split(','));
                }
                _groups.Add("");

                foreach (var item in jObject)
                {
                    String group = Convert.ToString(item.Value["memberGroupId"]);
                    long pId = Convert.ToInt64(item.Value["programmeId"]);
                    long parentid = Convert.ToInt64(item.Value["parentProgrammeId"]);
                    displayGroup=Convert.ToInt32(item.Value["displayGroup"]);
                    gameMode = Convert.ToInt32(item.Value["gameMode"]);

                    if (parentid == programmeId)
                    {
                        if (filterGroups == false || _groups.Contains(group))
                        {

                            int listingBannerImageHeight = CheckIntNull(item.Value["listingBannerImageHeight"]);
                            int listingBannerImageWeight = CheckIntNull(item.Value["listingBannerImageWeight"]);
                            int listingBackgroundImageHeight = CheckIntNull(item.Value["listingBackgroundImageHeight"]);
                            int listingBackgroundImageWeight = CheckIntNull(item.Value["listingBackgroundImageWeight"]);
                            int identityBannerImageHeight = CheckIntNull(item.Value["identityBannerImageHeight"]);
                            int identityBannerImageWeight = CheckIntNull(item.Value["identityBannerImageWeight"]);
                            int backgroundFooterImageHeight = CheckIntNull(item.Value["backgroundFooterImageHeight"]);
                            int backgroundFooterImageWeight = CheckIntNull(item.Value["backgroundFooterImageWeight"]);
                            int backgroundImageHeight = CheckIntNull(item.Value["backgroundImageHeight"]);
                            int backgroundImageWeight = CheckIntNull(item.Value["backgroundImageWeight"]);

                            Dictionary<string, object> programme = new Dictionary<string, object>() { 
                                {"programmeId",item.Value["programmeId"]},
                                {"programmeCode",item.Value["programmeCode"]},
                                {"title",item.Value["title"]},
                                {"showTime",item.Value["showTime"]},
                                {"displayGroup",item.Value["displayGroup"]},
                                {"gameMode",item.Value["gameMode"]},
                                {"channel",item.Value["channel"]},
                                //{"parentProgrammeId",item.Value["parentProgrammeId"]},
                                {"memberGroupId",item.Value["memberGroupId"]},
                                {"hasSubProgrammes",item.Value["hasSubProgrammes"]},

                                {"listingBannerImageUrl",item.Value["listingBannerImageUrl"]},
                                {"listingBackgroundImageUrl",item.Value["listingBackgroundImageUrl"]},
                                {"identityBannerImageUrl",item.Value["identityBannerImageUrl"]},

                                {"submitBtnImage",item.Value["submitBtnImage"]},
                                {"resetBtnImage",item.Value["resetBtnImage"]},
                                {"continueGameBtnImage",item.Value["continueGameBtnImage"]},
                                {"scoreDisplayBackgroundImage",item.Value["scoreDisplayBackgroundImage"]},
                                {"backgroundImage",item.Value["backgroundImage"]},
                                {"backgroundRepeatYImage",item.Value["backgroundRepeatYImage"]},
                                {"backgroundFooterImage",item.Value["backgroundFooterImage"]},

                                {"thumbnailUrl",item.Value["thumbnailUrl"]},
                                {"backgroundColorCode",item.Value["backgroundColorCode"]},
                                {"presentationType",item.Value["presentationType"]},
                                {"gamePageActiveFlag",item.Value["gamePageActiveFlag"]},

                                {"enableBorder",item.Value["enableBorder"]},
                                {"clickable",item.Value["clickable"]},
                                
                                {"hasRealTimeGameActiveFlag",item.Value["hasRealTimeGameActiveFlag"]},
                                {"listingBackgroudColorCode",item.Value["listingBackgroudColorCode"]},
                                {"activeTimeInterval",item.Value["activeTimeInterval"]},
                                {"isAuthPassword",item.Value["isAuthPassword"]},
                                
                                {"listingBannerImageSize",ConvertSize(listingBannerImageWeight,listingBannerImageHeight)},
                                {"listingBackgroundImageSize",ConvertSize(listingBackgroundImageWeight,listingBackgroundImageHeight)},
                                {"identityBannerImageSize",ConvertSize(identityBannerImageWeight,identityBannerImageHeight)},
                                {"backgroundFooterImageSize",ConvertSize(backgroundFooterImageWeight,backgroundFooterImageHeight)},
                                {"backgroundImageSize",ConvertSize(backgroundImageWeight,backgroundImageHeight)},

                                //{"isDisplayOptionName",item.Value["isDisplayOptionName"]},
                                //{"optionNameColorCode ",item.Value["optionNameColorCode "]},
                                //{"cbackgroundImage",item.Value["cbackgroundImage"]},
                                //{"alwaysSuccess",item.Value["alwaysSuccess"]},
                             };

                            filterList.Add(programme);
                        }
                    }
                    else if (pId == programmeId)
                    {
                        parentProgram = item.Value.ToObject<Dictionary<string, object>>();
                        //Dictionary<string, object> programme = new Dictionary<string, object>();
                        //foreach (var el in item.Value.Children())
                        //{
                        //    JProperty proelperty = el as JProperty;
                        //    programme.Add(proelperty.Name, proelperty.Value);
                        //}
                        //parentProgram = programme;
                    }
                }
            }
            if (parentProgram != null)
            {
                parentProgram["subProgrammes"] = filterList;
                return parentProgram;
            }
            else if (programmeId > 0)
            {
                //programmeId not found;

                Dictionary<string, object> defaultParentProgram = new Dictionary<string, object>() { 
                    {"programmeId",-1},
                    {"programmeCode",""},
                    {"title","Programme Not Found"},
                    {"hasSubProgrammes",0},
                    {"parentProgrammeId",-1},
                    {"displayGroup",0},
                    {"gameMode",0}
                 };
                return defaultParentProgram;
            }
            else
            {
                Object title = "";
                try
                {
                    String json = TVBCOM.SDK.Net.HttpLoader.Get(Microsoft.WindowsAzure.CloudConfigurationManager.GetSetting("LinkOfProgramme_0_Title"));
                    title = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(json).Values.First();
                }
                catch (Exception)
                {
                    title = "Big Big Fun";
                    //throw;
                }
                //root programme list; 
                //层级 programme_0
                Dictionary<string, object> defaultParentProgram = new Dictionary<string, object>() { 
                    {"programmeId",0},
                    {"programmeCode",""},
                    {"title",title},
                    {"hasSubProgrammes",1},
                    {"parentProgrammeId",0},
                    {"subProgrammes",filterList},
                    {"displayGroup",displayGroup},
                    {"gameMode",gameMode}
                 };
                return defaultParentProgram;
            }

        }

        public static object ConvertSize(int width, int height)
        {
            return new { width = width, height = height };
        }

        private static int CheckIntNull(object obj)
        {
            int result = 0;
            int.TryParse(obj.ToString(), out result);
            return result; ;
        }

    }
}
