﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microsoft.ServiceModel.Http
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.ServiceModel.Description;
    using Microsoft.Http;
    using Newtonsoft.Json;
    public class JsonNetProcessor : MediaTypeProcessor
    {
        private Type parameterType;
        public JsonNetProcessor(HttpOperationDescription operation, MediaTypeProcessorMode mode)
            : base(operation, mode)
        {
            if (this.Parameter != null)
            {
                this.parameterType = this.Parameter.ParameterType;
            }
        }
        public override IEnumerable<string> SupportedMediaTypes
        {
            get
            {
                return new List<string> { "text/json", "application/json" };
            }
        }
        public override void WriteToStream(object instance, Stream stream, HttpRequestMessage request)
        {
            var serializer = new JsonSerializer();

            using (var sw = new StreamWriter(stream))
            using (var writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, instance);
            }
        }
        public override object ReadFromStream(Stream stream, HttpRequestMessage request)
        {
            var serializer = new JsonSerializer();
            using (var sr = new StreamReader(stream))
            using (var reader = new JsonTextReader(sr))
            {
                var result = serializer.Deserialize(reader, parameterType);
                return result;
            }
        }
    }
}