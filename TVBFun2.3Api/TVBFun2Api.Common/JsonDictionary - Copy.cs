﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace TVBFun2Api.Common
{
    [Serializable]
    [ServiceKnownType(typeof(string))]
    [ServiceKnownType(typeof(int))]
    [ServiceKnownType(typeof(bool))]
    [ServiceKnownType(typeof(float))]
    [ServiceKnownType(typeof(JsonDictionary))]
    [ServiceKnownType(typeof(JsonList))] 
    public class JsonDictionary : ISerializable, IDictionary<string,object>
    {
        private Dictionary<string, object> m_entries;

        [IgnoreDataMember]
        public Dictionary<string, object> DataDictionary
        {
            get
            {
                return m_entries;
            }
        }

        public JsonDictionary()
        {
            m_entries = new Dictionary<string, object>();
        }

        [IgnoreDataMember]
        public IEnumerable<KeyValuePair<string, object>> Entries
        {
            get { return m_entries; }
        }

        protected JsonDictionary(SerializationInfo info, StreamingContext context)
        {
            m_entries = new Dictionary<string, object>();
            foreach (var entry in info)
            {
                m_entries.Add(entry.Name, entry.Value);
            }
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            foreach (var entry in m_entries)
            {
                info.AddValue(entry.Key, entry.Value);
            }
        }

        #region Dictionary_Start

        public void Add(string key, object value)
        {
            this.DataDictionary.Add(key, value);
        }

        public bool ContainsKey(string key)
        {
            return this.DataDictionary.ContainsKey(key);
        }

        public ICollection<string> Keys
        {
            get { return this.DataDictionary.Keys; }
        }

        public bool Remove(string key)
        {
            return this.DataDictionary.Remove(key);
        }

        public bool TryGetValue(string key, out object value)
        {
            return this.DataDictionary.TryGetValue(key, out value);
        }

        public ICollection<object> Values
        {
            get { return this.DataDictionary.Values; }
        }

        public object this[string key]
        {
            get
            {
                return this.DataDictionary[key];
            }
            set
            {
                this.DataDictionary[key] = value;
            }
        }

        public void Add(KeyValuePair<string, object> item)
        {
            this.DataDictionary.Add(item.Key,item.Value);
        }

        public void Clear()
        {
            this.DataDictionary.Clear();
        }

        public bool Contains(KeyValuePair<string, object> item)
        {
            return this.DataDictionary.Contains(item);
        }

        public void CopyTo(KeyValuePair<string, object>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get { return this.DataDictionary.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(KeyValuePair<string, object> item)
        {
            return this.DataDictionary.Remove(item.Key);
        }

        public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
        {
            return this.DataDictionary.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.DataDictionary.GetEnumerator();
        }

        #endregion
    }
}
