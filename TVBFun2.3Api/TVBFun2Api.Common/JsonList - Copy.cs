﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace TVBFun2Api.Common
{
    [Serializable]
    [ServiceKnownType(typeof(string))]
    [ServiceKnownType(typeof(int))]
    [ServiceKnownType(typeof(bool))]
    [ServiceKnownType(typeof(float))]
    [ServiceKnownType(typeof(JsonDictionary))]
    [ServiceKnownType(typeof(JsonList))] 
    public class JsonList : List<object>
    {
    }
}
