﻿using Microsoft.ApplicationServer.Caching;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TVBFun2Api.Common
{
    class BsonDataSerializer : IDataCacheObjectSerializer
    {
        public object Deserialize(System.IO.Stream stream)
        {
            using (stream)
            {
                JsonSerializer serializer = new JsonSerializer();
                using (BsonReader reader = new BsonReader(stream))
                {
                    return serializer.Deserialize(reader);
                }
            }
        }

        public void Serialize(System.IO.Stream stream, object value)
        {
            JsonSerializer serializer = new JsonSerializer();
            BsonWriter writer = new BsonWriter(stream);

            serializer.Serialize(writer, value);
            writer.Flush();
            //stream.Seek(0, System.IO.SeekOrigin.Begin);
        }
    }
}
