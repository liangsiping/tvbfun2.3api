﻿using Microsoft.ApplicationServer.Caching;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;

namespace TVBFun2Api.Common
{
    public class ValueConvert
    {
        public static String GetHMACSHA1Value(String password, String programId)
        {
            HMACSHA1 hmacsha1 = new HMACSHA1();
            hmacsha1.Key = Encoding.UTF8.GetBytes(password);
            byte[] dataBuffer = Encoding.UTF8.GetBytes(programId);
            byte[] hashBytes = hmacsha1.ComputeHash(dataBuffer);
            return Base16Encoder.Encode(hashBytes).ToLower();
        }

        public static int ToInt32(object obj)
        {

            try
            {
                return Convert.ToInt32(obj);
            }
            catch (Exception ex)
            {
                return default(Int32);
            }
        }
        public static Int64 ToInt64(object obj)
        {
            try
            {
                return Convert.ToInt64(obj);
            }
            catch (Exception ex)
            {
                return default(Int64);
            }
        }
        public static Int16 ToInt16(object obj)
        {
            try
            {
                return Convert.ToInt16(obj);
            }
            catch (Exception ex)
            {
                return default(Int16);
            }
        }

        public static double ToDouble(object obj)
        {
            try
            {
                return Convert.ToDouble(obj);
            }
            catch (Exception ex)
            {
                return default(Double);
            }
        }

        public static String ToString(object obj)
        {
            try
            {
                return Convert.ToString(obj);
            }
            catch (Exception ex)
            {
                return default(String);
            }
        }
        public static DateTime ToDateTime(object obj)
        {
            try
            {
                return Convert.ToDateTime(obj);
            }
            catch (Exception ex)
            {
                return new DateTime(1900, 1, 1);
            }
        }
        public static bool ToBoolean(object obj)
        {
            try
            {
                return Convert.ToBoolean(obj);
            }
            catch (Exception ex)
            {
                return default(Boolean);
            }
        }
    }
    //class LocalCacheItem {
    //    public string Key { get; set; }
    //    public object Value { get; set; }
    //    public DataCacheItemVersion Version { get; set; }
    //    public TimeSpan ExtensionTimeout { get; set; }
    //}
    public class Util
    {
        /******
        private static readonly DataCacheFactory cacheFactory = new DataCacheFactory();

        private static readonly Dictionary<string, LocalCacheItem> localCaches = new Dictionary<string, LocalCacheItem>();

        public static object GetCache(String key) {

            LocalCacheItem localCache = null;
            if (!localCaches.TryGetValue(key, out localCache)) {
                lock (localCaches)
                {
                    if (!localCaches.TryGetValue(key, out localCache))
                    {
                        localCache = new LocalCacheItem();
                        localCache.Key = key;
                        localCaches[key] = localCache;
                    }
                }
            }

            DataCache cache = cacheFactory.GetDefaultCache();


            if (localCache.Version == null)
            {
                lock (localCache)
                {
                    if (localCache.Version == null)
                    {
                        DataCacheItem dataCacheItem = cache.GetCacheItem(key);
                        localCache.ExtensionTimeout = dataCacheItem.ExtensionTimeout;
                        localCache.Version = dataCacheItem.Version;
                        localCache.Value = dataCacheItem.Value;

                    }
                    else
                    {
                        DataCacheItemVersion lastVersion = localCache.Version;
                        object newObj = cache.GetIfNewer(key, ref lastVersion);
                        if (newObj != null)
                        {
                            localCache.Version = lastVersion;
                            localCache.Value = newObj;
                        }
                    }
                }
            }
            else
            {
                DataCacheItemVersion lastVersion = localCache.Version;
                object newObj = cache.GetIfNewer(key, ref lastVersion);
                if (newObj != null)
                {
                    localCache.Version = lastVersion;
                    localCache.Value = newObj;
                }
            }

            return localCache.Value;
        }
        ******/

        private static Dictionary<String, CloudBlockBlob> list = new Dictionary<string, CloudBlockBlob>();

        public static String UpperFirstChar(String str)
        {
            char[] a = str.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }

        public static String LowerFirstChar(String str)
        {
            char[] a = str.ToCharArray();
            a[0] = char.ToLower(a[0]);
            return new string(a);
        }


        public static Stream CreateJsonResponseStream(Object obj)
        {
            var prop = new HttpResponseMessageProperty();

            MemoryStream ms = new MemoryStream();
            JsonSerializer serializer = new JsonSerializer();
            TextWriter writer = new StreamWriter(ms, Encoding.UTF8);
            serializer.Serialize(writer, obj);
            writer.Flush();
            ms.Seek(0, SeekOrigin.Begin);

            prop.Headers.Set("Content-Type", "application/json; charset=utf-8");
            prop.Headers.Set("Cache-Control", "public, max-age=60");
            prop.Headers.Set("Last-Modified", DateTime.UtcNow.AddSeconds(-1).ToString("R"));
            //prop.Headers.Set("Content-MD5", GetMd5Hash(ms.ToArray()));
            System.ServiceModel.OperationContext.Current.OutgoingMessageProperties.Add(HttpResponseMessageProperty.Name, prop);
            return ms;

            /*********
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(obj)));
            * *******/

        }
        public static T ObjectFromBinary<T>(byte[] b)
        {
            using (MemoryStream stream = new MemoryStream(b))
            {
                JsonSerializer serializer = new JsonSerializer();
                using (BsonReader reader = new BsonReader(stream))
                {
                    return (T)serializer.Deserialize(reader);
                }
            }
        }
        public static object ObjectFromBinary(byte[] b)
        {
            using (MemoryStream stream = new MemoryStream(b))
            {
                JsonSerializer serializer = new JsonSerializer();
                using (BsonReader reader = new BsonReader(stream))
                {
                    return serializer.Deserialize(reader);
                }
            }
        }


        public static byte[] ObjectToBinary(Object obj)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                JsonSerializer serializer = new JsonSerializer();
                using (BsonWriter writer = new BsonWriter(stream))
                {
                    serializer.Serialize(writer, obj);
                    writer.Flush();
                    return stream.ToArray();
                }
            }
        }

        public static string GetMd5Hash(byte[] input)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                // Convert the input string to a byte array and compute the hash. 
                byte[] data = md5Hash.ComputeHash(input);

                // Create a new Stringbuilder to collect the bytes 
                // and create a string.
                StringBuilder sBuilder = new StringBuilder();

                // Loop through each byte of the hashed data  
                // and format each one as a hexadecimal string. 
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }

                // Return the hexadecimal string. 
                return sBuilder.ToString();
            }
        }

        private static CloudBlockBlob GetCloudBlobClient(string connectionString, string containerName, string fileName, string mimeType)
        {
            CloudBlockBlob client;
            String key = connectionString + containerName + fileName + mimeType;

            if (!list.TryGetValue(key, out client))
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);

                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                // Retrieve reference to a previously created container.
                CloudBlobContainer container = blobClient.GetContainerReference(containerName);

                if (container.CreateIfNotExists())
                {
                    var permission = container.GetPermissions();
                    permission.PublicAccess = BlobContainerPublicAccessType.Container;
                    container.SetPermissions(permission);
                }

                Encoding utf8Encoding = new UTF8Encoding(false);
                // Retrieve reference to a blob named "myblob".
                client = container.GetBlockBlobReference(fileName);
                client.Properties.ContentType = mimeType;
                client.Properties.ContentEncoding = utf8Encoding.WebName;
                client.Properties.CacheControl = "public, max-age=" + CloudConfigurationManager.GetSetting("CDNCacheControlMaxAge");
                list.Add(key, client);
            }

            return client;
        }

        public static string UploadFile(object data, string connectionString, string containerName, string fileName, string mimeType)
        {
            CloudBlockBlob blockBlob = GetCloudBlobClient(connectionString, containerName, fileName, mimeType);

            using (MemoryStream stream = new MemoryStream())
            {
                Encoding utf8Encoding = new UTF8Encoding(false);
                JsonSerializer serializer = new JsonSerializer();
                using (TextWriter writer = new StreamWriter(stream, utf8Encoding))
                {

                    serializer.Serialize(writer, data);
                    writer.Flush();
                    stream.Seek(0, SeekOrigin.Begin);
                    //blockBlob.SetProperties();
                    blockBlob.UploadFromStream(stream);
                }
                blockBlob.SetProperties();
            }

            return blockBlob.Uri.AbsoluteUri;
        }

        public static List<String> DownloadAllFile(string connectionString, string containerName)
        {
            List<String> result = new List<string>();

            // Retrieve storage account from connection string.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
            // Create the blob client. 
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference(containerName);
            // Loop over items within the container and output the length and URI.
            foreach (IListBlobItem item in container.ListBlobs(null, false))
            {
                if (item.GetType() == typeof(CloudBlockBlob))
                {
                    CloudBlockBlob blob = (CloudBlockBlob)item;
                    //Console.WriteLine("Block blob of length {0}: {1}", blob.Properties.Length, blob.Uri);
                    result.Add(blob.Name);
                }
                else if (item.GetType() == typeof(CloudPageBlob))
                {
                    CloudPageBlob pageBlob = (CloudPageBlob)item;
                    //Console.WriteLine("Page blob of length {0}: {1}", pageBlob.Properties.Length, pageBlob.Uri);
                    result.Add(pageBlob.Name);
                }
                else if (item.GetType() == typeof(CloudBlobDirectory))
                {
                    CloudBlobDirectory directory = (CloudBlobDirectory)item;
                    //Console.WriteLine("Directory: {0}", directory.Uri);
                }
            }

            return result;
        }

        public static void DeleteFile(string connectionString, string containerName, string fileName)
        {
            // Retrieve storage account from connection string.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
               connectionString);
            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference(containerName);
            // Retrieve reference to a blob named "myblob.txt".
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);
            // Delete the blob.
            blockBlob.Delete();
        }
    }
}
