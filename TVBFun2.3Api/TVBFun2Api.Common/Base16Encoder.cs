﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TVBFun2Api.Common
{
    public class Base16Encoder
    {

        private static char[] HEX = new char[]{
        '0', '1', '2', '3', '4', '5', '6', '7',
        '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

        /**
         * Convert bytes to a base16 string.
         * 将字节转换为base16字符串。
         */
        public static String Encode(byte[] byteArray)
        {
            StringBuilder hexBuffer = new StringBuilder(byteArray.Length * 2);
            for (int i = 0; i < byteArray.Length; i++)
                for (int j = 1; j >= 0; j--)
                    hexBuffer.Append(HEX[(byteArray[i] >> (j * 4)) & 0xF]);
            return hexBuffer.ToString();
        }

        /**
         * Convert a base16 string into a byte array.
         */
        public static byte[] Decode(String s)
        {

            int len = s.Length;
            byte[] r = new byte[len / 2];
            for (int i = 0; i < r.Length; i++)
            {
                int digit1 = s.ElementAt(i * 2), digit2 = s.ElementAt(i * 2 + 1);
                if (digit1 >= '0' && digit1 <= '9')
                    digit1 -= '0';
                else if (digit1 >= 'A' && digit1 <= 'F')
                    digit1 -= 'A' - 10;
                if (digit2 >= '0' && digit2 <= '9')
                    digit2 -= '0';
                else if (digit2 >= 'A' && digit2 <= 'F')
                    digit2 -= 'A' - 10;

                r[i] = (byte)((digit1 << 4) + digit2);
            }
            return r;
        }
    }
}
