﻿using Microsoft.WindowsAzure;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using TVBCOM.SDK.Database;

namespace TVBFun2Api.Common.v1_8.DataModel
{
    [DataContract]
    public class ProgrammeInfoJsonForApp
    {
        [DataMember(Name="programmeInfoSet")]
        public List<ProgrammeInfo> ProgrammeInfoSet { get; set; }

        [DataMember(Name = "sysConfigVar")]
        public SystemConfigV18 SysConfigVar { get; set; }

        [DataMember(Name = "groupProgrammeInfoSet")]
        public List<ProgrammeInfo> GroupProgrammeInfoSet { get; set; }

        public static ProgrammeInfoJsonForApp LoadFromDB(String connectionString, String urlPrefix)
        {
            ProgrammeInfoJsonForApp programmeInfoSet = new ProgrammeInfoJsonForApp();

            programmeInfoSet.ProgrammeInfoSet = new List<ProgrammeInfo>();
            programmeInfoSet.GroupProgrammeInfoSet = new List<ProgrammeInfo>();
            Dictionary<string, List<OptionInfo>> optionsSet = new Dictionary<string, List<OptionInfo>>();
            using (DB db = new DB(DBProvider.BuiltInDBProvider.MSSQL, connectionString))
            {
                using (IDataReader reader = db.ExecuteReader("select * from OptionInfoView_v1_8 order by programmeId desc", CommandType.Text))
                {
                    while (reader.Read())
                    {
                        List<OptionInfo> list = null;
                        if (!optionsSet.TryGetValue(Convert.ToString(reader["programmeId"]), out list))
                        {
                            list = new List<OptionInfo>();
                            optionsSet.Add(Convert.ToString(reader["programmeId"]), list);
                        }
                        list.Add(OptionInfo.ParseFromReader(reader, urlPrefix));
                    }
                }
            }

            using (DB db = new DB(DBProvider.BuiltInDBProvider.MSSQL, connectionString))
            {
                using (IDataReader reader = db.ExecuteReader("GetProgrammeListWithOrder_v1_8", CommandType.StoredProcedure))
                {
                    while (reader.Read())
                    {
                        //Dictionary<string, object> programme = parseDictionaryFromReader(reader, urlPrefix);
                        ProgrammeInfo programme = ProgrammeInfo.ParseFromReader(reader, urlPrefix);
                        
                        List<OptionInfo> options = null;
                        String programmeId = Convert.ToString(programme.ProgrammeID);
                        if (optionsSet.TryGetValue(programmeId, out options))
                        {
                            programme.OptionInfo=options;
                        }
                        else
                        {
                            programme.OptionInfo = new List<OptionInfo>();
                        }

                        if (String.IsNullOrWhiteSpace(programme.BelongMemberGroup))
                        {
                            programmeInfoSet.ProgrammeInfoSet.Add(programme);
                        }
                        else
                        {
                            programme.BelongMemberGroup = ";" + programme.BelongMemberGroup.Trim(';') + ";";
                            programmeInfoSet.GroupProgrammeInfoSet.Add(programme);
                        }
                    }
                }
                //strb.AppendLine("Got programmes: " + watch.ElapsedMilliseconds);
            }
            return programmeInfoSet;
        }

    }
}
