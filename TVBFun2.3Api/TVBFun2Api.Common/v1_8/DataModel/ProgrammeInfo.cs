﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace TVBFun2Api.Common.v1_8.DataModel
{
    [DataContract]
    //[JsonConverter(typeof(CustomDateTimeConverter))]
    public class ProgrammeInfo
    {

        [DataMember]
        public int PassUserInfo;

        [DataMember]
        public int ProgrammeID { get; set; }

        [DataMember]
        public string ProgrammeCode { get; set; }

        [DataMember]
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime VotingStart { get; set; }

        [DataMember]
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime VotingTo { get; set; }

        [DataMember]
        public string ProgrammeTitle1TC { get; set; }

        [DataMember]
        public string ProgrammeTitle2TC { get; set; }

        [DataMember]
        public string ProgrammeSubTitleTC { get; set; }

        [DataMember]
        public string ProgrammeShowTime { get; set; }

        [DataMember]
        public string Channel { get; set; }

        [DataMember]
        public bool VotePageDisplayFlat { get; set; }

        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public string Logo { get; set; }

        [DataMember]
        public string Thumbnail { get; set; }

        [DataMember]
        public string Skin { get; set; }

        [DataMember]
        public string ChooseTextColor { get; set; }

        [DataMember]
        public string GameMessage { get; set; }

        [DataMember]
        public string GameURL { get; set; }

        [DataMember]
        public int GameMessageType { get; set; }

        [DataMember]
        public string PreVoteMessage { get; set; }

        [DataMember]
        public string PreVoteURL { get; set; }

        [DataMember]
        public string PostVoteMessage { get; set; }

        [DataMember]
        public string PostVoteURL { get; set; }

        [DataMember]
        public string GreetingMessage { get; set; }

        [DataMember]
        public int TemplateType { get; set; }

        [DataMember]
        public int LuckyDrawEvent { get; set; }

        [DataMember]
        public string LuckyDrawURL { get; set; }

        [DataMember]
        public List<OptionInfo> OptionInfo { get; set; }

        [DataMember]
        public int SortOrder { get; set; }

        [DataMember]
        public int MaxOptionSelect { get; set; }

        [DataMember]
        public bool AllowScrolling { get; set; }

        [DataMember]
        public int NumOfOptionEachLine { get; set; }

        [DataMember]
        public string ResultPageURL { get; set; }

        [DataMember]
        public object[] bannerSpotSet { get; set; }

        [DataMember]
        public int MasterProgrammeID { get; set; }

        [DataMember]
        public string GameQuestion { get; set; }

        [DataMember]
        public string ListingProgramImage { get; set; }

        [DataMember]
        public string ListingBackgroudImage { get; set; }

        [DataMember]
        public string BelongMemberGroup { get; set; }

        [DataMember]
        public int ParentProgrammeID { get; set; }

        [DataMember]
        public int IsParentProgramme { get; set; }

        [DataMember]
        public int SeriousVote { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public object[] OptionInfo_Config { get; set; }

        [DataMember]
        public int SeriousVoteForOther { get; set; }

        [DataMember]
        public string ProgrammeTandCLink { get; set; }

        [DataMember]
        public int OptionRandomlyDisplay { get; set; }

        [DataMember]
        public int IsRepeatVote { get; set; }

        [DataMember]
        public int EnableRealTimeResult { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string BannerSpot8Image { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string BannerSpot8Text { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string BannerSpot8ClickThroughLink { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int BannerSpot8BannerID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int EnableEncryptData { get; set; }

        [DataMember]
        public string RepeatVoteRemindMsg { get; set; }

        [DataMember]
        public string IntroURL { get; set; }

        [DataMember]
        public int CasualUserSessionValidPeriodInMins { get; set; }

        [DataMember]
        public int MinOptionSelect { get; set; }

        [DataMember]
        public int RepeatVoteIntervalInSec { get; set; }

        [DataMember]
        public int AutoEnterLuckydraw { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string AutoLuckydrawText { get; set; }

        [DataMember]
        public int AllowMemberOnly { get; set; }

        [DataMember]
        public int VotingTimeOutInSec { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ServerLinkVotingSecondary { get; set; }

        [DataMember]
        public int VoteSubmissionMaxRandomDelayInSec { get; set; }

        [DataMember]
        public int IsInstantLuckyDraw { get; set; }

        public static ProgrammeInfo ParseFromReader(IDataReader reader, String urlPrefix)
        {
            ProgrammeInfo programmeInfo = new ProgrammeInfo();
            try
            {
                DateTime utcNow = DateTime.UtcNow;
                programmeInfo.ProgrammeID = ValueConvert.ToInt32(reader["ProgrammeID"]);
                programmeInfo.ProgrammeCode = ValueConvert.ToString(reader["ProgrammeCode"]);
                programmeInfo.ProgrammeTitle1TC = ValueConvert.ToString(reader["ProgrammeTitle1TC"]);
                programmeInfo.ProgrammeTitle2TC = ValueConvert.ToString(reader["ProgrammeTitle2TC"]);
                programmeInfo.ProgrammeSubTitleTC = ValueConvert.ToString(reader["ProgrammeSubTitleTC"]);
                programmeInfo.ProgrammeShowTime = ValueConvert.ToString(reader["ProgrammeShowTime"]);
                programmeInfo.Channel = ValueConvert.ToString(reader["Channel"]);
                programmeInfo.VotePageDisplayFlat = DateTime.Compare(utcNow, ValueConvert.ToDateTime(reader["DTGamePageActiveStart"])) >= 0 && DateTime.Compare(utcNow, ValueConvert.ToDateTime(reader["DTGamePageActiveEnd"])) < 0;
                programmeInfo.Status = ValueConvert.ToInt32(reader["Status"]);
                programmeInfo.Logo = ValueConvert.ToString(reader["Logo"]);
                programmeInfo.Thumbnail = ValueConvert.ToString(reader["Thumbnail"]);
                programmeInfo.Skin = ValueConvert.ToString(reader["Skin"]);
                programmeInfo.ChooseTextColor = ValueConvert.ToString(reader["ChooseTextColor"]);
                programmeInfo.TemplateType = ValueConvert.ToInt32(reader["TemplateType"]);
                programmeInfo.LuckyDrawEvent = ValueConvert.ToInt32(reader["LuckyDrawEvent"]);
                programmeInfo.LuckyDrawURL = ValueConvert.ToString(reader["LuckyDrawURL"]);
                programmeInfo.GreetingMessage = ValueConvert.ToString(reader["GreetingMessage"]);
                if (DateTime.Compare(utcNow, ValueConvert.ToDateTime(reader["DTGamePageActiveStart"])) <= 0)
                {
                    programmeInfo.GameMessage = ValueConvert.ToString(reader["PreGameMessage"]);
                    programmeInfo.GameURL = ValueConvert.ToString(reader["PreGameURL"]);
                    programmeInfo.GameMessageType = 0;
                }
                else
                {
                    programmeInfo.GameMessage = ValueConvert.ToString(reader["PostGameMessage"]);
                    programmeInfo.GameURL = ValueConvert.ToString(reader["PostGameURL"]);
                    programmeInfo.GameMessageType = 1;
                }
                programmeInfo.PreVoteMessage = ValueConvert.ToString(reader["PreVoteMessage"]);
                programmeInfo.PreVoteURL = ValueConvert.ToString(reader["PreVoteURL"]);
                programmeInfo.PostVoteMessage = ValueConvert.ToString(reader["PostVoteMessage"]);
                programmeInfo.PostVoteURL = ValueConvert.ToString(reader["PostVoteURL"]);
                programmeInfo.SortOrder = ValueConvert.ToInt32(reader["SortOrder"]);
                programmeInfo.VotingStart = ValueConvert.ToDateTime(reader["DTVoteActiveStart"]);
                programmeInfo.VotingTo = ValueConvert.ToDateTime(reader["DTVoteActiveEnd"]);
                programmeInfo.AllowScrolling = ValueConvert.ToInt32(reader["AllowScrolling"]) == 1;
                programmeInfo.MaxOptionSelect = ValueConvert.ToInt32(reader["MaxOptionSelect"]);
                programmeInfo.NumOfOptionEachLine = ValueConvert.ToInt32(reader["NumOfOptionEachLine"]);
                //programmeInfo.ResultPageURL = ValueConvert.ToString( reader["ResultPageURL"]);
                programmeInfo.ResultPageURL = Path.Combine(urlPrefix, ValueConvert.ToString(reader["ResultPageURL"]));
                programmeInfo.MasterProgrammeID = ValueConvert.ToInt32(reader["MasterProgrammeID"]);
                programmeInfo.GameQuestion = ValueConvert.ToString(reader["GameQuestion"]);
                programmeInfo.ListingProgramImage = ValueConvert.ToString(reader["ListingProgramImage"]);
                programmeInfo.ListingBackgroudImage = ValueConvert.ToString(reader["ListingBackgroudImage"]);
                programmeInfo.BelongMemberGroup = ValueConvert.ToString(reader["BelongMemberGroup"]);
                programmeInfo.ParentProgrammeID = ValueConvert.ToInt32(reader["ParentProgrammeID"]);
                programmeInfo.IsParentProgramme = ValueConvert.ToInt32(reader["IsParentProgramme"]);
                programmeInfo.SeriousVote = ValueConvert.ToInt32(reader["IsSeriousVote"]);
                programmeInfo.SeriousVoteForOther = ValueConvert.ToInt32(reader["SeriousVoteForOther"]);
                programmeInfo.ProgrammeTandCLink = ValueConvert.ToString(reader["ProgrammeTandCLink"]);
                programmeInfo.OptionRandomlyDisplay = ValueConvert.ToInt32(reader["OptionRandomlyDisplay"]);
                programmeInfo.EnableRealTimeResult = ValueConvert.ToInt32(reader["EnableRealTimeResult"]);
                programmeInfo.IsRepeatVote = ValueConvert.ToInt32(reader["IsRepeatVote"]);
                programmeInfo.RepeatVoteRemindMsg = ValueConvert.ToString(reader["RepeatVoteRemindMsg"]);
                programmeInfo.IntroURL = ValueConvert.ToString(reader["IntroURL"]);
                programmeInfo.CasualUserSessionValidPeriodInMins = ValueConvert.ToInt32(reader["CasualUserSessionValidPeriodInMins"]);
                programmeInfo.PassUserInfo = ValueConvert.ToInt32(reader["PassUserInfo"]);
                programmeInfo.MinOptionSelect = ValueConvert.ToInt32(reader["MinOptionSelect"]);
                programmeInfo.RepeatVoteIntervalInSec = ValueConvert.ToInt32(reader["RepeatVoteIntervalInSec"]);
                programmeInfo.AutoEnterLuckydraw = ValueConvert.ToInt32(reader["AutoEnterLuckydraw"]);
                programmeInfo.AllowMemberOnly = ValueConvert.ToInt32(reader["AllowMemberOnly"]);
                programmeInfo.VotingTimeOutInSec = ValueConvert.ToInt32(reader["VotingTimeOutInSec"]);
                programmeInfo.AutoLuckydrawText = ValueConvert.ToString(reader["AutoLuckydrawText"]);
                programmeInfo.ServerLinkVotingSecondary = ValueConvert.ToString(reader["ServerLinkVotingSecondary"]);
                programmeInfo.VoteSubmissionMaxRandomDelayInSec = ValueConvert.ToInt32(reader["VoteSubmissionMaxRandomDelayInSec"]);
                programmeInfo.IsInstantLuckyDraw = ValueConvert.ToInt32(reader["IsInstantLuckyDraw"]);


                programmeInfo.ResultPageURL = Path.Combine(urlPrefix, ValueConvert.ToString(reader["RealTimeResultUrl"]));

                if (!String.IsNullOrWhiteSpace(programmeInfo.Logo))
                    programmeInfo.Logo = Path.Combine(urlPrefix, programmeInfo.Logo);

                //if (!String.IsNullOrWhiteSpace(programmeInfo.LuckyDrawURL))
                //    programmeInfo.LuckyDrawURL = Path.Combine(urlPrefix, programmeInfo.LuckyDrawURL);

                if (!String.IsNullOrWhiteSpace(programmeInfo.ListingBackgroudImage))
                    programmeInfo.ListingBackgroudImage = Path.Combine(urlPrefix, programmeInfo.ListingBackgroudImage);

                if (!String.IsNullOrWhiteSpace(programmeInfo.ListingProgramImage))
                    programmeInfo.ListingProgramImage = Path.Combine(urlPrefix, programmeInfo.ListingProgramImage);

                if (!String.IsNullOrWhiteSpace(programmeInfo.Thumbnail))
                    programmeInfo.Thumbnail = Path.Combine(urlPrefix, programmeInfo.Thumbnail);

            }
            catch (Exception ex)
            {
                programmeInfo = (ProgrammeInfo)null;
            }
            return programmeInfo;
        }
    }
}
