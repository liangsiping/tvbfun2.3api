﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using TVBCOM.SDK.Database;

namespace TVBFun2Api.Common.v1_8.DataModel
{
    [DataContract]
    public class Todayprogramme
    {
        [DataMember(Name = "programmeInfoSet")]
        public List<ProgrammeInfo> ProgrammeInfoSet { get; set; }

        [DataMember(Name = "sysConfigVar")]
        public SystemConfigV18 SysConfigVar { get; set; }

        [DataMember(Name = "groupProgrammeInfoSet")]
        public List<ProgrammeInfo> GroupProgrammeInfoSet { get; set; }

        public static ProgrammeInfoJsonForApp LoadFromDB(String connectionString, String urlPrefix)
        {
            ProgrammeInfoJsonForApp programmeInfoSet = new ProgrammeInfoJsonForApp();

            programmeInfoSet.ProgrammeInfoSet = new List<ProgrammeInfo>();
            programmeInfoSet.GroupProgrammeInfoSet = new List<ProgrammeInfo>();
            Dictionary<string, List<OptionInfo>> optionsSet = new Dictionary<string, List<OptionInfo>>();
            using (DB db = new DB(DBProvider.BuiltInDBProvider.MSSQL, connectionString))
            {
                String sql = @"SELECT a.* FROM [dbo].[OptionInfo] a 
WHERE exists ( select 1 from ProgrammeInfo c where c.programmeId= a.programmeId) 
order by a.programmeId desc";
                using (IDataReader reader = db.ExecuteReader(sql, CommandType.Text))
                {
                    while (reader.Read())
                    {
                        List<OptionInfo> list = null;
                        if (!optionsSet.TryGetValue(Convert.ToString(reader["programmeId"]), out list))
                        {
                            list = new List<OptionInfo>();
                            optionsSet.Add(Convert.ToString(reader["programmeId"]), list);
                        }
                        list.Add(OptionInfo.ParseFromReader(reader, urlPrefix));
                    }
                }
            }

            using (DB db = new DB(DBProvider.BuiltInDBProvider.MSSQL, connectionString))
            {
                String sql = @"
	   SELECT [ProgrammeID]
      ,[ProgrammeCode]
      ,isnull([ProgrammeTitle1TC],'') AS [ProgrammeTitle1TC]
      ,isnull([ProgrammeTitle2TC],'') AS [ProgrammeTitle2TC]
      ,isnull([ProgrammeSubTitleTC],'') AS [ProgrammeSubTitleTC]
      ,[ProgrammeShowTime]
      ,[Channel]
      ,[DTVotingServerDeploymentStart]
      ,[DTVotingServerDeploymentEnd]
      ,[DTListingActiveStart]
      ,[DTListingActiveEnd]
      ,[DTGamePageActiveStart]
      ,[DTGamePageActiveEnd]
      ,[DTVoteActiveStart]
      ,[DTVoteActiveEnd]
      ,[Status]
      ,[Logo]
      ,[Thumbnail]
      ,[PreGameMessage]
      ,[PreGameURL]
      ,[PostGameMessage]
      ,[PostGameURL]
      ,[PreVoteMessage]
      ,[PreVoteURL]
      ,[PostVoteMessage]
      ,[PostVoteURL]
      ,[Skin]
      ,[ChooseTextColor]
      ,[GreetingMessage]
      ,[TemplateType]
      ,[LuckyDrawEvent]
      ,[LuckyDrawURL]
      ,[SortOrder]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[ModifitedBy]
      ,[ModifitedDate]
      ,[LuckyDrawQuestion]
      ,[LuckyDrawAnswerA]
      ,[LuckyDrawAnswerB]
      ,[RawDataGenerated]
      ,[MaxOptionSelect]
      ,[AllowScrolling]
      ,[NumOfOptionEachLine]
      ,[ResultPageURL]
      ,[ChooseTextColorLine2]
      ,[MasterProgrammeID]
      ,[GameQuestion]
      ,[ListingProgramImage]
      ,[ListingBackgroudImage]
      ,[BelongMemberGroup]
      ,[ParentProgrammeID]
      ,[IsParentProgramme]
      ,[IsSeriousVote]
      ,[SeriousVoteForOther]
      ,[ProgrammeTandCLink]
      ,[OptionRandomlyDisplay]
      ,[EnableRealTimeResult]
      ,[EncrpytStorageInfo]
      ,[IsRepeatVote]
      ,[RepeatVoteRemindMsg]
      ,[CustomResultImg]
      ,[IntroURL]
      ,[CasualUserSessionValidPeriodInMins]
      ,[PassUserInfo]
      ,[MinOptionSelect]
      ,[RepeatVoteIntervalInSec]
      ,[AutoEnterLuckydraw]
      ,[AllowMemberOnly]
      ,[VotingTimeOutInSec]
      ,[ServerLinkVotingSecondary]
      ,[VoteSubmissionMaxRandomDelayInSec]
      ,[AutoLuckydrawText]
      ,[IsInstantLuckyDraw]
	  ,replace('tvbfunhtml/pregen/voteresult/{0}.html','{0}',[ProgrammeID]) RealTimeResultUrl
  FROM [dbo].[ProgrammeInfo]
	WHERE DTVoteActiveEnd  between '" + DateTime.Now.ToShortDateString() + " 00:00:00' and '" + DateTime.Now.ToShortDateString() + " 23:59:59'";

                using (IDataReader reader = db.ExecuteReader(sql, CommandType.Text))
                {
                    while (reader.Read())
                    {
                        ProgrammeInfo programme = ProgrammeInfo.ParseFromReader(reader, urlPrefix);
                        List<OptionInfo> options = null;
                        String programmeId = Convert.ToString(programme.ProgrammeID);
                        if (optionsSet.TryGetValue(programmeId, out options))
                        {
                            programme.OptionInfo = options;
                        }
                        else
                        {
                            programme.OptionInfo = new List<OptionInfo>();
                        }

                        if (String.IsNullOrWhiteSpace(programme.BelongMemberGroup))
                        {
                            programmeInfoSet.ProgrammeInfoSet.Add(programme);
                        }
                        else
                        {
                            programme.BelongMemberGroup = ";" + programme.BelongMemberGroup.Trim(';') + ";";
                            programmeInfoSet.GroupProgrammeInfoSet.Add(programme);
                        }
                    }
                }
            }
            return programmeInfoSet;
        }
    }
}
