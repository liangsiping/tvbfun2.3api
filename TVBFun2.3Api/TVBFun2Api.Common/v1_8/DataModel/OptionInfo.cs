﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace TVBFun2Api.Common.v1_8.DataModel
{
    [DataContract]
    //[JsonConverter(typeof(CustomDateTimeConverter))]
    public class OptionInfo
    {
        [DataMember]
        public int OptionID { get; set; }

        [DataMember]
        public string OptionNameTC { get; set; }

        [DataMember]
        public string OptionImage { get; set; }

        [DataMember]
        public string OptionSound { get; set; }

        [DataMember]
        public int SortOrder { get; set; }

        [DataMember]
        public string OptionURL { get; set; }

        [DataMember]
        public string OptionMediaURL { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string OptionImageHeight { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string OptionImageWidth { get; set; }

        //public int CreatedBy { get; set; }

        //public DateTime CreatedDate { get; set; }

        //public int ModifitedBy { get; set; }

        //public DateTime ModifitedDate { get; set; }

        //public int ProgrammeID { get; set; }

        //public int AlwaysWinOption { get; set; }

        public static OptionInfo ParseFromReader(IDataReader reader, String urlPrefix) {
            OptionInfo optionInfo = new OptionInfo();
            try
            {
                optionInfo.OptionID = ValueConvert.ToInt32( reader["OptionID"]);
                optionInfo.OptionNameTC = ValueConvert.ToString( reader["OptionNameTC"]);
                optionInfo.OptionImage = ValueConvert.ToString( reader["OptionImage"]);
                optionInfo.OptionSound = ValueConvert.ToString( reader["OptionSound"]);
                optionInfo.OptionURL = ValueConvert.ToString( reader["OptionURL"]);
                optionInfo.OptionMediaURL = ValueConvert.ToString( reader["OptionMediaURL"]);
                //optionInfo.ProgrammeID = ValueConvert.ToInt32( reader["ProgrammeID"]);
                //optionInfo.AlwaysWinOption = ValueConvert.ToInt32( reader["AlwaysWinOption"]);
                optionInfo.SortOrder = ValueConvert.ToInt32( reader["SortOrder"]);
                //optionInfo.CreatedBy = ValueConvert.ToInt32( reader["CreatedBy"]);
                //optionInfo.CreatedDate = ValueConvert.ToDateTime( reader["CreatedDate"]);
                //optionInfo.ModifitedBy = ValueConvert.ToInt32( reader["ModifitedBy"]);
                //optionInfo.ModifitedDate = ValueConvert.ToDateTime( reader["ModifitedDate"]);
                if (!String.IsNullOrWhiteSpace(optionInfo.OptionImage))
                {
                    optionInfo.OptionImage = Path.Combine(urlPrefix, optionInfo.OptionImage);
                }
            }
            catch (Exception ex)
            {
                optionInfo = null;
            }
            return optionInfo;
        }
    }
}
