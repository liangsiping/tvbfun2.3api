﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TVBFun2Api.Common.v1_8.DataModel
{
    public class SystemConfigV18 : Dictionary<string, object>
    {
        public SystemConfigV18(Dictionary<string, object> configDict) {
            foreach (var item in configDict) {
                this[Util.UpperFirstChar(item.Key)] = item.Value;
            }
        }
    }
}
