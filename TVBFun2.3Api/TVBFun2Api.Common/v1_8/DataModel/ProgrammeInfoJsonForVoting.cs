﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace TVBFun2Api.Common.v1_8.DataModel
{
    public class ProgrammeInfoJsonForVoting : List<ProgrammeInfoForVoting>
    {
        public ProgrammeInfoJsonForVoting() { 
        
        }

        public void AddAll(List<ProgrammeInfo> programmeInfos)
        {
            foreach (var item in programmeInfos)
            {
                this.Add(new ProgrammeInfoForVoting(item));
            }
        }

        public ProgrammeInfoJsonForVoting(List<ProgrammeInfo> programmeInfos) {
            this.AddAll(programmeInfos);
        }
    }

    public class CustomDateTimeConverter : IsoDateTimeConverter
    {
        public CustomDateTimeConverter()
        {
            base.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
        }
    }

    /*****
     * AllowDupVote: true,
EncrpytionEnable: false,
ParentProgrammeID: 0,
ProgrammeCode: "general",
ProgrammeID: 4139,
ProgrammeOption: [ ],
ProgrammeTitleTC: "ä¸€æ­¥é€šTVBå»£å‘Šå¥—è£ 2014 ",
RepeatVoteIntervalInSec: 0,
TemplateType: 5,
VotingStart: "2013-12-31 16:00:00",
VotingTo: "2014-12-31 15:59:00"
     * ****/
    [DataContract]
    public class ProgrammeInfoForVoting
    {
        private ProgrammeInfo programmeInfo;
        public ProgrammeInfoForVoting(ProgrammeInfo programmeInfo)
        {
            this.programmeInfo = programmeInfo;
        }

        [DataMember]
        public bool AllowDupVote { get { return Convert.ToBoolean(programmeInfo.IsRepeatVote); } }

        [DataMember]
        public bool EncrpytionEnable { get { return Convert.ToBoolean(programmeInfo.EnableEncryptData); } }

        [DataMember]
        public int ParentProgrammeID { get { return programmeInfo.ParentProgrammeID; } }

        [DataMember]
        public string ProgrammeCode { get { return programmeInfo.ProgrammeCode; } }

        [DataMember]
        public int ProgrammeID { get { return programmeInfo.ProgrammeID; } }

        [DataMember]
        public List<Dictionary<string, object>> ProgrammeOption
        {
            get
            {
                List<Dictionary<string, object>> options = new List<Dictionary<string, object>>();
                foreach (var option in this.programmeInfo.OptionInfo)
                {
                    Dictionary<string, object> dict = new Dictionary<string, object>()
               {
                   
                   {"OptionID",option.OptionID},

                   {"OptionNameTC",option.OptionNameTC}
                   
               };
                    options.Add(dict);
                }
                return options;
            }
        }

        [DataMember]
        public string ProgrammeTitleTC { get {
            //return "Title "+ProgrammeID;
            return programmeInfo.ProgrammeTitle1TC;
        } }


            
        [DataMember]
        public int RepeatVoteIntervalInSec { get { return programmeInfo.RepeatVoteIntervalInSec; } }

        [DataMember]
        public int TemplateType { get { return programmeInfo.TemplateType; } }

        [DataMember]
        //[JsonConverter(typeof(CustomDateTimeConverter))]
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime VotingStart { get { return programmeInfo.VotingStart; } }

        [DataMember]
        //[JsonConverter(typeof(CustomDateTimeConverter))]
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime VotingTo { get { return programmeInfo.VotingTo; } }
    }
}
