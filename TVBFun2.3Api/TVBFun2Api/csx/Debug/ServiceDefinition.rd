﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="TVBFun2Api" generation="1" functional="0" release="0" Id="1fcb8ce8-6574-4bec-88e6-da995bbad876" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="TVBFun2ApiGroup" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="TVBFun2Api.Admin.Service:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/TVBFun2Api/TVBFun2ApiGroup/LB:TVBFun2Api.Admin.Service:Endpoint1" />
          </inToChannel>
        </inPort>
        <inPort name="TVBFun2Api.Service:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/TVBFun2Api/TVBFun2ApiGroup/LB:TVBFun2Api.Service:Endpoint1" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="TVBFun2Api.Admin.Service:CDNCacheControlMaxAge" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Admin.Service:CDNCacheControlMaxAge" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Admin.Service:CMSDBConnection" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Admin.Service:CMSDBConnection" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Admin.Service:EnableGenerateStaticFile" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Admin.Service:EnableGenerateStaticFile" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Admin.Service:EnableUploadBak" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Admin.Service:EnableUploadBak" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Admin.Service:EnableUploadFTP" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Admin.Service:EnableUploadFTP" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Admin.Service:EncryptProgrammeJsonNameFormat" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Admin.Service:EncryptProgrammeJsonNameFormat" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Admin.Service:FTPStorageHost" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Admin.Service:FTPStorageHost" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Admin.Service:FTPStoragePassword" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Admin.Service:FTPStoragePassword" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Admin.Service:FTPStorageUser" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Admin.Service:FTPStorageUser" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Admin.Service:image_url_colud" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Admin.Service:image_url_colud" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Admin.Service:JSON_v1_8_Folder" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Admin.Service:JSON_v1_8_Folder" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Admin.Service:JSON_v1_8_VOTING_Folder" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Admin.Service:JSON_v1_8_VOTING_Folder" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Admin.Service:JSON_v2_3_Folder" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Admin.Service:JSON_v2_3_Folder" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Admin.Service:LinkOfForceUpgradeAndroid" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Admin.Service:LinkOfForceUpgradeAndroid" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Admin.Service:LinkOfForceUpgradeIOS" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Admin.Service:LinkOfForceUpgradeIOS" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Admin.Service:LinkOfForceUpgradeWp" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Admin.Service:LinkOfForceUpgradeWp" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Admin.Service:LinkOfProgramme_0_Title" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Admin.Service:LinkOfProgramme_0_Title" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Admin.Service:LinkOfSystemConfig" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Admin.Service:LinkOfSystemConfig" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Admin.Service:Microsoft.WindowsAzure.Plugins.Caching.ClientDiagnosticLevel" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Admin.Service:Microsoft.WindowsAzure.Plugins.Caching.ClientDiagnosticLevel" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Admin.Service:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Admin.Service:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Admin.Service:NotifyFileCopyUrl" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Admin.Service:NotifyFileCopyUrl" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Admin.Service:StorageConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Admin.Service:StorageConnectionString" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Admin.Service:StorageConnectionStringBak" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Admin.Service:StorageConnectionStringBak" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Admin.Service:UrlPrefix" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Admin.Service:UrlPrefix" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Admin.ServiceInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Admin.ServiceInstances" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.CacheSizePercentage" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.CacheSizePercentage" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.ConfigStoreConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.ConfigStoreConnectionString" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.DiagnosticLevel" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.DiagnosticLevel" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.NamedCaches" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.NamedCaches" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.CacheInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.CacheInstances" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Service:Microsoft.WindowsAzure.Plugins.Caching.ClientDiagnosticLevel" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Service:Microsoft.WindowsAzure.Plugins.Caching.ClientDiagnosticLevel" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.Service:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.Service:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="TVBFun2Api.ServiceInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/TVBFun2Api/TVBFun2ApiGroup/MapTVBFun2Api.ServiceInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:TVBFun2Api.Admin.Service:Endpoint1">
          <toPorts>
            <inPortMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.Service/Endpoint1" />
          </toPorts>
        </lBChannel>
        <lBChannel name="LB:TVBFun2Api.Service:Endpoint1">
          <toPorts>
            <inPortMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Service/Endpoint1" />
          </toPorts>
        </lBChannel>
        <sFSwitchChannel name="SW:TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheArbitrationPort">
          <toPorts>
            <inPortMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Cache/Microsoft.WindowsAzure.Plugins.Caching.cacheArbitrationPort" />
          </toPorts>
        </sFSwitchChannel>
        <sFSwitchChannel name="SW:TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheClusterPort">
          <toPorts>
            <inPortMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Cache/Microsoft.WindowsAzure.Plugins.Caching.cacheClusterPort" />
          </toPorts>
        </sFSwitchChannel>
        <sFSwitchChannel name="SW:TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheReplicationPort">
          <toPorts>
            <inPortMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Cache/Microsoft.WindowsAzure.Plugins.Caching.cacheReplicationPort" />
          </toPorts>
        </sFSwitchChannel>
        <sFSwitchChannel name="SW:TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheServicePortInternal">
          <toPorts>
            <inPortMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Cache/Microsoft.WindowsAzure.Plugins.Caching.cacheServicePortInternal" />
          </toPorts>
        </sFSwitchChannel>
        <sFSwitchChannel name="SW:TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheSocketPort">
          <toPorts>
            <inPortMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Cache/Microsoft.WindowsAzure.Plugins.Caching.cacheSocketPort" />
          </toPorts>
        </sFSwitchChannel>
      </channels>
      <maps>
        <map name="MapTVBFun2Api.Admin.Service:CDNCacheControlMaxAge" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.Service/CDNCacheControlMaxAge" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Admin.Service:CMSDBConnection" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.Service/CMSDBConnection" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Admin.Service:EnableGenerateStaticFile" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.Service/EnableGenerateStaticFile" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Admin.Service:EnableUploadBak" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.Service/EnableUploadBak" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Admin.Service:EnableUploadFTP" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.Service/EnableUploadFTP" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Admin.Service:EncryptProgrammeJsonNameFormat" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.Service/EncryptProgrammeJsonNameFormat" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Admin.Service:FTPStorageHost" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.Service/FTPStorageHost" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Admin.Service:FTPStoragePassword" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.Service/FTPStoragePassword" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Admin.Service:FTPStorageUser" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.Service/FTPStorageUser" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Admin.Service:image_url_colud" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.Service/image_url_colud" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Admin.Service:JSON_v1_8_Folder" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.Service/JSON_v1_8_Folder" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Admin.Service:JSON_v1_8_VOTING_Folder" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.Service/JSON_v1_8_VOTING_Folder" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Admin.Service:JSON_v2_3_Folder" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.Service/JSON_v2_3_Folder" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Admin.Service:LinkOfForceUpgradeAndroid" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.Service/LinkOfForceUpgradeAndroid" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Admin.Service:LinkOfForceUpgradeIOS" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.Service/LinkOfForceUpgradeIOS" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Admin.Service:LinkOfForceUpgradeWp" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.Service/LinkOfForceUpgradeWp" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Admin.Service:LinkOfProgramme_0_Title" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.Service/LinkOfProgramme_0_Title" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Admin.Service:LinkOfSystemConfig" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.Service/LinkOfSystemConfig" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Admin.Service:Microsoft.WindowsAzure.Plugins.Caching.ClientDiagnosticLevel" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.Service/Microsoft.WindowsAzure.Plugins.Caching.ClientDiagnosticLevel" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Admin.Service:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.Service/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Admin.Service:NotifyFileCopyUrl" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.Service/NotifyFileCopyUrl" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Admin.Service:StorageConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.Service/StorageConnectionString" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Admin.Service:StorageConnectionStringBak" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.Service/StorageConnectionStringBak" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Admin.Service:UrlPrefix" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.Service/UrlPrefix" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Admin.ServiceInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.ServiceInstances" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.CacheSizePercentage" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Cache/Microsoft.WindowsAzure.Plugins.Caching.CacheSizePercentage" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.ConfigStoreConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Cache/Microsoft.WindowsAzure.Plugins.Caching.ConfigStoreConnectionString" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.DiagnosticLevel" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Cache/Microsoft.WindowsAzure.Plugins.Caching.DiagnosticLevel" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.NamedCaches" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Cache/Microsoft.WindowsAzure.Plugins.Caching.NamedCaches" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.CacheInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.CacheInstances" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Service:Microsoft.WindowsAzure.Plugins.Caching.ClientDiagnosticLevel" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Service/Microsoft.WindowsAzure.Plugins.Caching.ClientDiagnosticLevel" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.Service:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Service/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapTVBFun2Api.ServiceInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.ServiceInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="TVBFun2Api.Admin.Service" generation="1" functional="0" release="0" software="E:\TVBCOM\TVB-Project\TVBFun2.3Api\TVBFun2Api\csx\Debug\roles\TVBFun2Api.Admin.Service" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="-1" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="8080" />
              <outPort name="TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheArbitrationPort" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/TVBFun2Api/TVBFun2ApiGroup/SW:TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheArbitrationPort" />
                </outToChannel>
              </outPort>
              <outPort name="TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheClusterPort" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/TVBFun2Api/TVBFun2ApiGroup/SW:TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheClusterPort" />
                </outToChannel>
              </outPort>
              <outPort name="TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheReplicationPort" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/TVBFun2Api/TVBFun2ApiGroup/SW:TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheReplicationPort" />
                </outToChannel>
              </outPort>
              <outPort name="TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheServicePortInternal" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/TVBFun2Api/TVBFun2ApiGroup/SW:TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheServicePortInternal" />
                </outToChannel>
              </outPort>
              <outPort name="TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheSocketPort" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/TVBFun2Api/TVBFun2ApiGroup/SW:TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheSocketPort" />
                </outToChannel>
              </outPort>
            </componentports>
            <settings>
              <aCS name="CDNCacheControlMaxAge" defaultValue="" />
              <aCS name="CMSDBConnection" defaultValue="" />
              <aCS name="EnableGenerateStaticFile" defaultValue="" />
              <aCS name="EnableUploadBak" defaultValue="" />
              <aCS name="EnableUploadFTP" defaultValue="" />
              <aCS name="EncryptProgrammeJsonNameFormat" defaultValue="" />
              <aCS name="FTPStorageHost" defaultValue="" />
              <aCS name="FTPStoragePassword" defaultValue="" />
              <aCS name="FTPStorageUser" defaultValue="" />
              <aCS name="image_url_colud" defaultValue="" />
              <aCS name="JSON_v1_8_Folder" defaultValue="" />
              <aCS name="JSON_v1_8_VOTING_Folder" defaultValue="" />
              <aCS name="JSON_v2_3_Folder" defaultValue="" />
              <aCS name="LinkOfForceUpgradeAndroid" defaultValue="" />
              <aCS name="LinkOfForceUpgradeIOS" defaultValue="" />
              <aCS name="LinkOfForceUpgradeWp" defaultValue="" />
              <aCS name="LinkOfProgramme_0_Title" defaultValue="" />
              <aCS name="LinkOfSystemConfig" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.Caching.ClientDiagnosticLevel" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="NotifyFileCopyUrl" defaultValue="" />
              <aCS name="StorageConnectionString" defaultValue="" />
              <aCS name="StorageConnectionStringBak" defaultValue="" />
              <aCS name="UrlPrefix" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;TVBFun2Api.Admin.Service&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;TVBFun2Api.Admin.Service&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;TVBFun2Api.Cache&quot;&gt;&lt;e name=&quot;Microsoft.WindowsAzure.Plugins.Caching.cacheArbitrationPort&quot; /&gt;&lt;e name=&quot;Microsoft.WindowsAzure.Plugins.Caching.cacheClusterPort&quot; /&gt;&lt;e name=&quot;Microsoft.WindowsAzure.Plugins.Caching.cacheReplicationPort&quot; /&gt;&lt;e name=&quot;Microsoft.WindowsAzure.Plugins.Caching.cacheServicePortInternal&quot; /&gt;&lt;e name=&quot;Microsoft.WindowsAzure.Plugins.Caching.cacheSocketPort&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;TVBFun2Api.Service&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[20000,20000,20000]" defaultSticky="true" kind="Directory" />
              <resourceReference name="TVBFun2Api.Admin.Service.svclog" defaultAmount="[1000,1000,1000]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.ServiceInstances" />
            <sCSPolicyUpdateDomainMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.ServiceUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.ServiceFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
        <groupHascomponents>
          <role name="TVBFun2Api.Cache" generation="1" functional="0" release="0" software="E:\TVBCOM\TVB-Project\TVBFun2.3Api\TVBFun2Api\csx\Debug\roles\TVBFun2Api.Cache" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaWorkerHost.exe " memIndex="-1" hostingEnvironment="consoleroleadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Microsoft.WindowsAzure.Plugins.Caching.cacheArbitrationPort" protocol="tcp" />
              <inPort name="Microsoft.WindowsAzure.Plugins.Caching.cacheClusterPort" protocol="tcp" />
              <inPort name="Microsoft.WindowsAzure.Plugins.Caching.cacheReplicationPort" protocol="tcp" />
              <inPort name="Microsoft.WindowsAzure.Plugins.Caching.cacheServicePortInternal" protocol="tcp" />
              <inPort name="Microsoft.WindowsAzure.Plugins.Caching.cacheSocketPort" protocol="tcp" />
              <outPort name="TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheArbitrationPort" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/TVBFun2Api/TVBFun2ApiGroup/SW:TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheArbitrationPort" />
                </outToChannel>
              </outPort>
              <outPort name="TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheClusterPort" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/TVBFun2Api/TVBFun2ApiGroup/SW:TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheClusterPort" />
                </outToChannel>
              </outPort>
              <outPort name="TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheReplicationPort" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/TVBFun2Api/TVBFun2ApiGroup/SW:TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheReplicationPort" />
                </outToChannel>
              </outPort>
              <outPort name="TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheServicePortInternal" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/TVBFun2Api/TVBFun2ApiGroup/SW:TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheServicePortInternal" />
                </outToChannel>
              </outPort>
              <outPort name="TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheSocketPort" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/TVBFun2Api/TVBFun2ApiGroup/SW:TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheSocketPort" />
                </outToChannel>
              </outPort>
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Caching.CacheSizePercentage" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.Caching.ConfigStoreConnectionString" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.Caching.DiagnosticLevel" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.Caching.NamedCaches" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;TVBFun2Api.Cache&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;TVBFun2Api.Admin.Service&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;TVBFun2Api.Cache&quot;&gt;&lt;e name=&quot;Microsoft.WindowsAzure.Plugins.Caching.cacheArbitrationPort&quot; /&gt;&lt;e name=&quot;Microsoft.WindowsAzure.Plugins.Caching.cacheClusterPort&quot; /&gt;&lt;e name=&quot;Microsoft.WindowsAzure.Plugins.Caching.cacheReplicationPort&quot; /&gt;&lt;e name=&quot;Microsoft.WindowsAzure.Plugins.Caching.cacheServicePortInternal&quot; /&gt;&lt;e name=&quot;Microsoft.WindowsAzure.Plugins.Caching.cacheSocketPort&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;TVBFun2Api.Service&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[20000,20000,20000]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.CacheInstances" />
            <sCSPolicyUpdateDomainMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.CacheUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.CacheFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
        <groupHascomponents>
          <role name="TVBFun2Api.Service" generation="1" functional="0" release="0" software="E:\TVBCOM\TVB-Project\TVBFun2.3Api\TVBFun2Api\csx\Debug\roles\TVBFun2Api.Service" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="-1" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="80" />
              <outPort name="TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheArbitrationPort" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/TVBFun2Api/TVBFun2ApiGroup/SW:TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheArbitrationPort" />
                </outToChannel>
              </outPort>
              <outPort name="TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheClusterPort" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/TVBFun2Api/TVBFun2ApiGroup/SW:TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheClusterPort" />
                </outToChannel>
              </outPort>
              <outPort name="TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheReplicationPort" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/TVBFun2Api/TVBFun2ApiGroup/SW:TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheReplicationPort" />
                </outToChannel>
              </outPort>
              <outPort name="TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheServicePortInternal" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/TVBFun2Api/TVBFun2ApiGroup/SW:TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheServicePortInternal" />
                </outToChannel>
              </outPort>
              <outPort name="TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheSocketPort" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/TVBFun2Api/TVBFun2ApiGroup/SW:TVBFun2Api.Cache:Microsoft.WindowsAzure.Plugins.Caching.cacheSocketPort" />
                </outToChannel>
              </outPort>
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Caching.ClientDiagnosticLevel" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;TVBFun2Api.Service&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;TVBFun2Api.Admin.Service&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;TVBFun2Api.Cache&quot;&gt;&lt;e name=&quot;Microsoft.WindowsAzure.Plugins.Caching.cacheArbitrationPort&quot; /&gt;&lt;e name=&quot;Microsoft.WindowsAzure.Plugins.Caching.cacheClusterPort&quot; /&gt;&lt;e name=&quot;Microsoft.WindowsAzure.Plugins.Caching.cacheReplicationPort&quot; /&gt;&lt;e name=&quot;Microsoft.WindowsAzure.Plugins.Caching.cacheServicePortInternal&quot; /&gt;&lt;e name=&quot;Microsoft.WindowsAzure.Plugins.Caching.cacheSocketPort&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;TVBFun2Api.Service&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[20000,20000,20000]" defaultSticky="true" kind="Directory" />
              <resourceReference name="TVBFun2Api.Service.svclog" defaultAmount="[1000,1000,1000]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.ServiceInstances" />
            <sCSPolicyUpdateDomainMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.ServiceUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.ServiceFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="TVBFun2Api.ServiceUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyUpdateDomain name="TVBFun2Api.Admin.ServiceUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyUpdateDomain name="TVBFun2Api.CacheUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="TVBFun2Api.Admin.ServiceFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyFaultDomain name="TVBFun2Api.CacheFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyFaultDomain name="TVBFun2Api.ServiceFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="TVBFun2Api.Admin.ServiceInstances" defaultPolicy="[1,1,1]" />
        <sCSPolicyID name="TVBFun2Api.CacheInstances" defaultPolicy="[1,1,1]" />
        <sCSPolicyID name="TVBFun2Api.ServiceInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="db7c092a-e124-4ddc-aeea-46eb88641582" ref="Microsoft.RedDog.Contract\ServiceContract\TVBFun2ApiContract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="7addcbf6-8b20-4a3f-aa10-944ed9e787a4" ref="Microsoft.RedDog.Contract\Interface\TVBFun2Api.Admin.Service:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Admin.Service:Endpoint1" />
          </inPort>
        </interfaceReference>
        <interfaceReference Id="b8e04912-dd2a-4504-a8b6-eee2368ae69e" ref="Microsoft.RedDog.Contract\Interface\TVBFun2Api.Service:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/TVBFun2Api/TVBFun2ApiGroup/TVBFun2Api.Service:Endpoint1" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>