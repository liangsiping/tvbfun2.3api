﻿using Microsoft.ApplicationServer.Caching;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Channels;
using System.ServiceModel.Web;
using System.Text;
using TVBFun2Api.Common;

namespace TVBFun2Api.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AppService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select AppService.svc or AppService.svc.cs at the Solution Explorer and start debugging.

    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AppService : IAppService
    {
        private static readonly DataCacheFactory cacheFactory = new DataCacheFactory();

        [AspNetCacheProfile("CacheGetProgrammes")]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = "/programmes?groups={groups}")]
        public Stream GetProgrammes(String groups)
        {
            return this.GetSubProgrammes("0", groups);
        }

        /// <summary>
        /// tvbfun最后一个页面 例如投票页面
        /// </summary>
        /// <param name="programmeId"></param>
        /// <returns></returns>
        [AspNetCacheProfile("CacheGetSubProgrammes")]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = "/programme_{programmeId}.json")]
        public Stream GetProgrammesByProgrammeId(String programmeId)
        {
            return this.GetSubProgrammes(programmeId, false, null);
        }

        [AspNetCacheProfile("CacheGetSubProgrammes")]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = "/programmes/{programmeId}?groups={groups}")]
        public Stream GetSubProgrammes(String programmeId, String groups) 
        {
            return GetSubProgrammes(programmeId, true, groups);
        }

        private Stream GetSubProgrammes(String programmeId, bool filterGroups, String groups)
        {
            long _programmeId = Convert.ToInt64(programmeId);
            
            DataCache cache = cacheFactory.GetDefaultCache();
            JObject jObject = (JObject)cache.Get("programmeList");

            return Util.CreateJsonResponseStream(ProgrammeHelper.FilterById(jObject, _programmeId, filterGroups, groups));
        }
    }
}
