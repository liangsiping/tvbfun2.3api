﻿using Microsoft.ApplicationServer.Caching;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using TVBFun2Api.Common;

namespace TVBFun2Api.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AppServiceCDN" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select AppServiceCDN.svc or AppServiceCDN.svc.cs at the Solution Explorer and start debugging.

    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AppServiceCDN : IAppServiceCDN
    {

        private static readonly DataCacheFactory cacheFactory = new DataCacheFactory();
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = "/programme_{programmeId}.json")]
        public Stream GetProgrammesByProgrammeId(String programmeId)
        {
            long _programmeId = Convert.ToInt64(programmeId);

            DataCache cache = cacheFactory.GetDefaultCache();
            JObject jObject = (JObject)cache.Get("programmeList");

            return Util.CreateJsonResponseStream(ProgrammeHelper.FilterById(jObject, _programmeId, false, null));
        }
    }
}
