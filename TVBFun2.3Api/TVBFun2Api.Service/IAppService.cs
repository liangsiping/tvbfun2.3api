﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using TVBFun2Api.Common;

namespace TVBFun2Api.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IAppService" in both code and config file together.
    [ServiceContract]
    public interface IAppService
    {
        [OperationContract]
        Stream GetSubProgrammes(String programmeId, String groups);
        
        [OperationContract]
        Stream GetProgrammesByProgrammeId(String programmeId);

        [OperationContract]
        Stream GetProgrammes(String groups);
    }
}
